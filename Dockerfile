FROM python:3.12-alpine

# Prevents Python from writing pyc files.
ENV PYTHONDONTWRITEBYTECODE=1

# Keeps Python from buffering stdout and stderr to avoid situations where
# the application crashes without emitting any logs due to buffering.
ENV PYTHONUNBUFFERED=1

WORKDIR /app

# Install crontab
RUN echo "5 * * * * /usr/local/bin/python /app/fetch.py 2>&1" > /etc/crontabs/root

# Install deps (mount pyproject.toml here to keep the layer small)
RUN --mount=type=cache,target=/root/.cache/pip \
    --mount=type=bind,source=pyproject.toml,target=pyproject.toml \
    python -m pip install .

# Copy files
COPY . .

# Run crond as main process of container
CMD [ "crond", "-f", "-l", "10", "-L", "/dev/stdout" ]
