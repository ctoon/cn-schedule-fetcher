from .discord_hooks import Webhook


class ShowColors:
    def __init__(self, config):
        self.d_url = config["discord"]["issues"]
        self.reported = []

        # From CNSA's new colors map
        self.theList = {
            # broken
            "??? (show id": ["ffffff", "000000"],
            "adult swim": ["000000", "bfbfbf"],  # Adult Swim
            "cartoon network programming": ["ffffff", "000000"],
            "cartoon network special edition": ["000000", "bfbfbf"],
            "cartoon network": ["ffffff", "000000"],
            "no data available": ["ffffff", "000000"],
            "to be announced": ["ffffff", "000000"],
            # main
            "12 oz. mouse": ["ffffff", "6665dc"],  # 12 Oz. Mouse
            "a christmas story christmas": ["000000", "bfbfbf"],  # A Christmas Story Christmas
            "adult swim yule log": ["ffffff", "000000"],  # Adult Swim Yule Log
            "adventure time: stakes!": ["ffffff", "a61d25"],  # Adventure Time: Stakes!
            "adventure time": ["ffffff", "a61d25"],
            "amazing world of gumball": ["000000", "49c1e4"],
            "american dad!": ["ffffff", "ef1c23"],
            "animated adventures of jack decker": ["ef070c", "0d6ab6"],
            "apollo gauntlet": ["9e944a", "881c35"],
            "apple & onion": ["f66e7a", "77e592"],
            "aqua something you know whatever": ["ddd9b1", "b82717"],
            "aqua teen hunger force forever": ["ddd9b1", "b82717"],
            "aqua teen hunger force": ["ddd9b1", "b82717"],
            "aqua teen": ["ddd9b1", "b82717"],
            "aqua tv show show": ["ddd9b1", "b82717"],
            "aqua unit patrol squad 1": ["ddd9b1", "b82717"],
            "as seen on adult swim": ["696969", "babfbb"],
            "baby looney tunes": ["f8ee12", "c9090c"],
            "bakugan": ["d71c23", "c4c5c7"],
            "ballmastrz": ["fdfa85", "ea271c"],
            "beef house": ["e6b12d", "002e2b"],
            "ben 10 (2017)": ["000000", "94c93d"],
            "ben 10 classic": ["8ec83c", "11181f"],
            "ben 10: alien swarm": ["000000", "95c93d"],
            "ben 10": ["000000", "94c93d"],
            "billy and mandy": ["de3d32", "040208"],
            "bing": ["000000", "72ddf6"],
            "birdgirl": ["ddc358", "58275e"],
            "black dynamite": ["e10384", "271c1c"],
            "black jesus": ["f4c598", "815667"],
            "blippi wonders": ["5385d7", "ffffff"],
            "bloodfeast": ["ff0000", "000000"],
            "bob's burgers": ["ed202d", "f4e023"],
            "boondocks": ["dfd965", "2d2e26"],
            "brad neely's harg nallin' sclopio peepio": ["fecc08", "fe00fa"],
            "brak show": ["c12756", "b9e1f7"],
            "bugs bunny builders": ["09b3e0", "ffffff"],  # Review color later
            "bunnicula": ["790603", "f76a12"],
            "caillou": ["c00000", "ffffff"],
            "care bears": ["e479b2", "4a2b6f"],
            "check it out!": ["4aec96", "1a62a7"],
            "childrens hospital": ["d30e15", "ffffff"],
            "china, il": ["000004", "ff983d"],
            "chowder": ["f2f2f2", "652d89"],
            "clarence": ["6340a1", "6bcb42"],
            "cleveland show": ["ffffff", "b371af"],
            "cloudy with a chance of meatballs": ["ffffff", "cb1714"],
            "cocomelon": ["000000", "ff8a88"],
            "codename": ["d1ba62", "000000"],
            "courage the cowardly dog": ["000000", "dbb4cb"],
            "cow and chicken": ["f6592a", "ffffff"],  # Cow And Chicken
            "craig of the creek": ["000000", "90ae58"],
            "dc super hero girls": ["e72131", "2cc4f5"],
            "decker": ["ffffff", "ef070c"],
            "development meeting": ["ffffff", "000000"],
            "dexter's laboratory": ["ffffff", "f47221"],
            "digikiss": ["696969", "babfbb"],
            "dorothy and the wizard of oz": ["f3ee13", "11744b"],
            "dr. seuss' the lorax": ["fdfdfd", "e98c2d"],
            "dream corp llc": ["2380b5", "ffffff"],
            "eagleheart": ["ffffff", "d0b261"],
            "ed, edd n eddy": ["efd484", "513d7b"],
            "elliott from earth": ["0ed043", "b13625"],
            "eric andre show": ["fff8e6", "e96645"],
            "evil con carne": ["000000", "d82304"],  # Evil con Carne
            "family guy": ["000000", "00a1d4"],
            "final space": ["eceee8", "6e5aa5"],
            "fishcenter": ["696d69", "babfbb"],
            "foster's home for imaginary friends": ["11b5e9", "000000"],
            "frisky dingo": ["e58753", "932929"],
            "futurama": ["e44047", "8ddac6"],
            "gemusetto machu picchu": ["ffffff", "fe4701"],
            "gemusetto": ["ffffff", "fe4701"],
            "genndy tartakovsky's primal": ["ffffff", "641300"],
            "gits": ["6ff14c", "0e2327"],
            "grizzy & the lemmings": ["f4c38e", "b35c2d"],
            "harley quinn": ["c00702", "de8fe0"],
            "harvey birdman, attorney at law": ["ddc358", "58275e"],
            "home movies": ["f58743", "ffffff"],
            "hot package": ["f6de2a", "a90f76"],
            "hot streets": ["4fd8dc", "d25a3b"],
            "i am weasel": ["d82304", "00377c"],  # I Am Weasel
            "infinity train": ["82db8b", "000000"],
            "infomercials": ["587994", "f7f7f7"],
            "jamir at home": ["000000", "bfbfbf"],
            "jellystone! vip": ["dbf44b", "#7c1d34"],
            "jellystone!": ["dbf44b", "#7c1d34"],
            "jessica's big little world": ["785f89", "ace7e7"],  # Jessica's Big Little World
            "jj villard's fairy tales": ["fff835", "7c1507"],
            "joe pera talks with you": ["2d3866", "ece3df"],
            "justice league action": ["f8ed51", "3a52a3"],
            "justice league": ["f8ed51", "3a52a3"],
            "king of the hill": ["fdf10f", "ef1a30"],
            "lamput": ["ffffff", "ff8600"],
            "lazor wulf": ["000000", "fad1e3"],
            "lego batman": ["ffffff", "021637"],
            "lego nexo knights": ["ffffff", "021637"],
            "lego ninjago": ["ffffff", "385624"],
            "lellobee city farm": ["bf7b47", "ffffff"],
            "let's go pocoyo": ["fad0a3", "3795ce"],
            "loiter squad": ["ecd277", "f15424"],
            "looney tunes cartoons": ["f8ee12", "c9090c"],
            "looney tunes": ["f8ee12", "c9090c"],
            "love monster": ["ffffff", "e84050"],
            "lucas the spider": ["1d1917", "487997"],
            "lucy": ["ff0000", "000000"],
            "mao mao, heroes of pure heart": ["64ea27", "cc0000"],
            "mary shelley's frankenhole": ["d0b434", "070201"],  # Review color later
            "mecha builders": ["dede62", "054f46"],
            "meet the batwheels": ["ffc000", "262626"],
            "mega man": ["b3e2f4", "18469e"],
            "metalocalypse": ["9d0000", "000000"],
            "mighty magiswords": ["243775", "78d2f7"],
            "mike tyson mysteries": ["f9cb20", "da4429"],
            "momma named me sheriff": ["7bd349", "bd3240"],
            "moral orel": ["ffffff", "2da4ca"],
            "mostly 4 millennials": ["e92983", "7ddde7"],
            "mr. neighbor's house 2": ["f0f638", "ffffff"],
            "mr. pickles": ["7bd349", "bd3240"],
            "mush-mush and the mushables": ["e2d078", "9b6e6d"],
            "my adventures with superman": ["f11712", "ffe63a"],  # My Adventures with Superman
            "my knight and me": ["884a15", "e3a333"],
            "neon joe, werewolf hunter": ["79f906", "000000"],  # Neon Joe, Werewolf Hunter
            "new looney tunes": ["f8ee12", "c9090c"],
            "ninjago": ["ffffff", "375623"],
            "ntsf": ["ffffff", "082930"],
            "off the air": ["000000", "ffff00"],
            "ok k.o.! let's be heroes": ["f1fe15", "c200fe"],
            "ole bud's anu football weekly": ["000000", "ffffff"],
            "over the garden wall": ["d9c190", "fdfdfd"],
            "peanuts": ["000000", "ffd204"],
            "pocoyo": ["fad0a3", "3795ce"],
            "power players": ["fcf18e", "ff3300"],
            "power rangers": ["bc2224", "ffffbd"],  # Power Rangers
            "powerpuff girls": ["000000", "f97eb5"],
            "regular show": ["ffffff", "582852"],
            "rick and morty": ["00b2c8", "000000"],
            "robot chicken": ["d9d9da", "b30810"],
            "royal crackers": ["ffcc00", "252f70"],  # Royal Crackers
            "samurai jack": ["f8e1a9", "7c4702"],
            "scooby-doo, where are you!": ["fedb00", "b22a96"],  # Scooby-Doo, Where Are You!
            "scooby-doo! and guess who?": ["ffffff", "9b0043"],  # Scooby-Doo! and Guess Who?
            "scooby-doo! frankencreepy": ["7f2d85", "f4af54"],
            "scooby-doo! mystery inc": ["7f2d85", "f4af54"],
            "scooby-doo!": ["7f2d85", "f4af54"],
            "sealab 2021": ["e6d96c", "0076b8"],
            "smiling friends": ["ffffff", "252525"],
            "space ghost coast to coast": ["000000", "fedb74"],
            "squidbillies": ["000000", "91ac8b"],
            "steven universe future": ["fce04e", "f04937"],
            "steven universe sing-a-long": ["fce04e", "f04937"],
            "steven universe": ["fce04e", "f04937"],
            "summer camp island": ["000000", "e6d0bb"],
            "super mansion": ["267ec9", "d1c0c9"],
            "superjail": ["f9c906", "71324d"],
            "supermansion": ["267ec9", "d1c0c9"],
            "teen titans go!": ["000000", "ffff00"],
            "teen titans": ["ffff00", "000000"],
            "teenage euthanasia": ["f64dd9", "191919"],
            "ten year old tom": ["ffffff", "f0df75"],  # Ten Year Old Tom
            "tender touches": ["0000ff", "cdcdff"],
            "the amazing world of gumball": ["000000", "49c1e4"],
            "the boondocks": ["ddd462", "2d2e26"],
            "the brak show": ["c51230", "72cef5"],  # The Brak Show
            "the carbonaro effect": ["0047e2", "ffffff"],
            "the cleveland show": ["ffffff", "b371af"],
            "the eric andre show": ["fff8e6", "e96645"],
            "the fungies!": ["f68da3", "50202f"],
            "the grim adventures of billy and mandy": ["de3d32", "040208"],
            "the heart she holler": ["faf532", "163e72"],  # Review color later
            "the heroic quest of the valiant prince ivandoe": ["000000", "fbe5b3"],  # The Heroic Quest of the Valiant Prince Ivandoe
            "the jellies": ["000000", "75e5fa"],
            "the looney tunes show": ["f8ee12", "c9090c"],  # The Looney Tunes Show
            "the marvelous misadventures of flapjack": ["cf1e25", "fce5bd"],
            "the new scooby-doo mysteries": ["f78724", "0a4440"],  # The New Scooby-Doo Mysteries
            "the new sd movies": ["7f2d85", "f4af54"],
            "the not-too-late show with elmo": ["ffffff", "041c8d"],
            "the powerpuff girls": ["000000", "f97eb5"],
            "the shivering truth": ["ffffff", "000000"],
            "the sylvester & tweety mysteries": ["af0c08", "642487"],  # The Sylvester & Tweety Mysteries
            "the venture brothers": ["ffffff", "89201b"],
            "the yogi bear show": ["158cae", "bda687"],  # The Yogi Bear Show
            "thomas & friends": ["f20000", "72ddf6"],
            "three busy debras": ["cc3595", "ffe3f5"],
            "thundercats roar!": ["91c0f1", "931127"],
            "thundercats roar": ["91c0f1", "931127"],
            "tig n' seek": ["e2553a", "ffd83f"],
            "tigtone": ["b9bbb8", "37251b"],
            "tim and eric awesome show 10 year anniversary tour": ["f6e23d", "2060e6"],
            "tim and eric awesome show, great job": ["f6e23d", "2060e6"],
            "tim and eric awesome": ["f6e23d", "2060e6"],
            "tim and eric's bedtime stories": ["f6e23d", "2060e6"],
            "tiny toons looniversity": ["008ffd", "fdfdfd"],  # Tiny Toons Looniversity
            "tom & jerry (apac shorts)": ["f0c852", "7d1511"],  # Tom & Jerry (APAC Shorts)
            "tom & jerry kids": ["f0c852", "7d1511"],  # Tom & Jerry Kids
            "tom & jerry show": ["f0c852", "7d1511"],
            "tom & jerry tales": ["f0c852", "7d1511"],
            "tom & jerry": ["f0c852", "7d1511"],
            "total drama action": ["d01319", "000000"],
            "total drama daycare": ["d69b00", "482e1d"],
            "total drama island": ["cb9605", "482e1d"],
            "total drama": ["643613", "d69900"],
            "total dramarama": ["d2a9f1", "4e0091"],
            "transformers cyberverse": ["c80b2c", "ffffff"],
            "transformers": ["c80b2c", "ffffff"],
            "tropical cop tales": ["e3c5f7", "f84615"],
            "ttg v ppg": ["000000", "ffff00"],
            "tuca & bertie": ["60f8ad", "ff8ccd"],
            "uncle grandpa": ["000000", "f68b1f"],
            "unicorn": ["f60003", "040404"],  # Unicorn: Warriors Eternal
            "unikitty": ["ffffff", "dc1e9a"],
            "venture brothers ": ["ffffff", "89201b"],
            "victor and valentino": ["eb1b33", "ecd377"],
            "wacky races": ["cf3b3d", "ffffff"],
            "we baby bears": ["eeeddd", "da5c45"],
            "we bare bears": ["dc5e48", "efedde"],
            "what's new scooby-doo?": ["6d287b", "eff0ac"],  # What's New Scooby-Doo?
            "williams stream": ["696d69", "babfbb"],
            "williams street swap shop": ["696d69", "babfbb"],
            "xavier": ["ffffff", "6874a4"],
            "yolo crystal fantasy": ["c258c7", "f0c580"],
            "yolo": ["c880fc", "7135c4"],  # YOLO: Silver Destiny
            "your pretty face is going to hell": ["f7715f", "8e1511"],
            # toonami: 17b7dd, 1b0120
            "assassination classroom": ["17b7dd", "1b0120"],
            "attack on titan": ["17b7dd", "1b0120"],
            "black clover": ["17b7dd", "1b0120"],
            "boruto": ["17b7dd", "1b0120"],
            "cowboy bebop": ["17b7dd", "1b0120"],
            "demon slayer": ["17b7dd", "1b0120"],
            "dr. stone": ["17b7dd", "1b0120"],
            "dragon ball super": ["17b7dd", "1b0120"],
            "dragon ball z kai": ["17b7dd", "1b0120"],
            "fena": ["17b7dd", "1b0120"],
            "fire force": ["17b7dd", "1b0120"],
            "flcl": ["17b7dd", "1b0120"],
            "food wars!": ["17b7dd", "1b0120"],
            "housing complex c": ["17b7dd", "1b0120"],
            "hunter x hunter": ["17b7dd", "1b0120"],
            "igpx": ["17b7dd", "1b0120"],  # IGPX
            "jojo's bizarre adventure": ["17b7dd", "1b0120"],
            "lupin the 3rd part 4": ["17b7dd", "1b0120"],
            "lupin the 3rd part 6": ["17b7dd", "1b0120"],
            "lycoris recoil": ["17b7dd", "1b0120"],  # Lycoris Recoil
            "made in abyss": ["17b7dd", "1b0120"],
            "megalobox": ["17b7dd", "1b0120"],
            "mob psycho 100": ["17b7dd", "1b0120"],
            "mobile suit gundam": ["17b7dd", "1b0120"],
            "my hero academia": ["17b7dd", "1b0120"],
            "naruto": ["17b7dd", "1b0120"],
            "ninja kamui": ["17b7dd", "1b0120"],  # Ninja Kamui
            "one piece": ["17b7dd", "1b0120"],
            "one-punch man": ["17b7dd", "1b0120"],
            "paranoia agent": ["17b7dd", "1b0120"],
            "pop team epic": ["17b7dd", "1b0120"],
            "shenmue": ["17b7dd", "1b0120"],
            "space dandy": ["17b7dd", "1b0120"],
            "special anime short": ["17b7dd", "1b0120"],
            "ssss.gridman": ["17b7dd", "1b0120"],
            "sword art online alicization war of underworld": ["17b7dd", "1b0120"],
            "the promised neverland": ["17b7dd", "1b0120"],
            "toonami block": ["17b7dd", "1b0120"],  # Toonami Block
            "toonami interstitial": ["17b7dd", "1b0120"],
            "yashahime": ["17b7dd", "1b0120"],
            "zom 100": ["17b7dd", "1b0120"],  # Zom 100: Bucket List of the Dead
            # movies, sitcoms, specials and weird: 000000, bfbfbf
            "8-bit christmas": ["000000", "bfbfbf"],  # 8-Bit Christmas
            "17 again": ["000000", "bfbfbf"],
            "a christmas story": ["000000", "bfbfbf"],
            "adult swim smalls presents": ["000000", "bfbfbf"],
            "adult swim smalls": ["000000", "bfbfbf"],  # Adult Swim SMALLS
            "adult swim x ben & jerry's present holy calamavote": ["000000", "bfbfbf"],
            "adventure time bmo": ["ffffff", "a61d25"],
            "again": ["000000", "bfbfbf"],
            "agent, the madagascar": ["000000", "bfbfbf"],
            "alice through the looking glass": ["000000", "bfbfbf"],  # Alice Through the Looking Glass
            "all-star superman": ["000000", "bfbfbf"],  # All-Star Superman
            "aloha, scooby-doo!": ["000000", "bfbfbf"],  # Aloha, Scooby-Doo!
            "alvin and the chipmunks": ["000000", "bfbfbf"],
            "ambient swim": ["000000", "bfbfbf"],  # Ambient Swim
            "aphex twin": ["000000", "bfbfbf"],
            "aquaman": ["000000", "bfbfbf"],
            "arthur christmas": ["000000", "bfbfbf"],  # Arthur Christmas
            "babe": ["000000", "bfbfbf"],  # Babe
            "back to the future part ii": ["000000", "bfbfbf"],
            "back to the future part iii": ["000000", "bfbfbf"],
            "back to the future": ["000000", "bfbfbf"],
            "bag boy": ["000000", "bfbfbf"],
            "batman & robin": ["000000", "bfbfbf"],  # Batman & Robin
            "batman forever": ["000000", "bfbfbf"],  # Batman Forever
            "batman ninja": ["000000", "bfbfbf"],
            "batman returns": ["000000", "bfbfbf"],  # Batman Returns
            "batman": ["000000", "bfbfbf"],
            "be cool, scooby-doo!": ["000000", "bfbfbf"],  # Be Cool, Scooby-Doo!
            "beast boy's that's what's up": ["000000", "bfbfbf"],
            "beetlejuice": ["000000", "bfbfbf"],  # Beetlejuice
            "ben 10 vs. the universe": ["000000", "bfbfbf"],
            "ben vs. the universe": ["000000", "bfbfbf"],
            "better than you madagascar": ["000000", "bfbfbf"],
            "beyond the overpass madagascar": ["000000", "bfbfbf"],
            "big top scooby-doo!": ["000000", "bfbfbf"],  # Big Top Scooby-Doo!
            "black out 2022": ["000000", "bfbfbf"],
            "blade runner 2049": ["000000", "bfbfbf"],
            "blade runner": ["000000", "bfbfbf"],
            "bono's peter and the wolf": ["000000", "bfbfbf"],  # Bono's Peter and The Wolf
            "breakfast puss in boots": ["000000", "bfbfbf"],
            "buffy the vampire slayer": ["000000", "bfbfbf"],  # Buffy the Vampire Slayer
            "cartoonito shorts": ["000000", "bfbfbf"],  # Cartoonito Shorts
            "charlie and the chocolate factory": ["000000", "bfbfbf"],
            "chill out, scooby-doo!": ["000000", "bfbfbf"],  # Chill Out, Scooby-Doo!
            "craig before the creek": ["000000", "bfbfbf"],  # Craig Before the Creek: An Original Movie
            "crazy rich asians": ["000000", "bfbfbf"],
            "daddy day care": ["000000", "bfbfbf"],
            "daylight savings time adjustment - spring": ["000000", "bfbfbf"],
            "daylight savings time adjustment - you are now springing forward...": ["000000", "bfbfbf"],
            "despicable me 2": ["000000", "bfbfbf"],
            "despicable me 3": ["000000", "bfbfbf"],
            "despicable me": ["000000", "bfbfbf"],
            "detective pikachu": ["000000", "bfbfbf"],
            "early man": ["000000", "bfbfbf"],  # Early Man
            "ed, edd n eddy's boo-haw haw": ["000000", "bfbfbf"],  # Ed, Edd n Eddy's Boo-Haw Haw
            "ed, edd n eddy's jingle, jingle, jangle": ["000000", "bfbfbf"],  # Ed, Edd n Eddy's Jingle, Jingle, Jangle
            "eggland": ["000000", "bfbfbf"],  # Eggland
            "elf pets": ["000000", "bfbfbf"],
            "elf": ["000000", "bfbfbf"],
            "ella enchanted": ["000000", "bfbfbf"],  # Ella Enchanted
            "eric andre live near broadway": ["fff8e6", "e96645"],  # Eric Andre Live Near Broadway
            "family matters": ["000000", "bfbfbf"],
            "family weekend movies": ["000000", "bfbfbf"],
            "family weekend": ["000000", "bfbfbf"],
            "fantastic mr. fox": ["000000", "bfbfbf"],  # Fantastic Mr. Fox
            "foster's home for imaginary friends christmas special": ["000000", "bfbfbf"],
            "four christmases": ["000000", "bfbfbf"],  # Four Christmases
            "fred claus": ["000000", "bfbfbf"],
            "garfield": ["000000", "bfbfbf"],
            "gigglefudge, usa": ["000000", "bfbfbf"],  # Gigglefudge, USA
            "godzilla vs. kong": ["000000", "bfbfbf"],  # Godzilla vs. Kong
            "gremlins": ["000000", "bfbfbf"],  # Gremlins
            "gulliver's travels": ["000000", "bfbfbf"],
            "happy feet two": ["000000", "bfbfbf"],
            "happy halloween, scooby-doo!": ["000000", "bfbfbf"],
            "harry potter 20th anniversary": ["000000", "bfbfbf"],
            "harry potter": ["000000", "bfbfbf"],
            "hotel for dogs": ["000000", "bfbfbf"],
            "how to train your dragon": ["000000", "bfbfbf"],
            "hunky boys go ding-dong": ["000000", "bfbfbf"],
            "injustice": ["000000", "bfbfbf"],  # Injustice
            "iron maiden": ["000000", "bfbfbf"],
            "jack frost": ["000000", "bfbfbf"],  # Jack Frost
            "jack stauber's opal": ["000000", "bfbfbf"],
            "joe pera helps you find the perfect christmas tree": ["2d3866", "ece3df"],  # Joe Pera Helps You Find The Perfect Christmas Tree
            "journey 2": ["000000", "bfbfbf"],
            "journey to the center of the earth": ["000000", "bfbfbf"],
            "justice league vs the fatal five": ["000000", "bfbfbf"],  # Justice League vs the Fatal Five
            "justice league vs. teen titans": ["000000", "bfbfbf"],  # Justice League vs. Teen Titans
            "king star king": ["000000", "bfbfbf"],  # King Star King
            "king tweety": ["000000", "bfbfbf"],  # King Tweety
            "kung fu panda 2": ["000000", "bfbfbf"],
            "kung fu panda": ["000000", "bfbfbf"],
            "legally blonde": ["000000", "bfbfbf"],  # Legally Blonde
            "lego dc batman": ["000000", "bfbfbf"],
            "lego dc comics super heroes": ["000000", "bfbfbf"],
            "lego dc comics superheroes": ["000000", "bfbfbf"],
            "lego dc shazam": ["000000", "bfbfbf"],
            "lego dc super heroes": ["000000", "bfbfbf"],
            "lego dc": ["000000", "bfbfbf"],
            "lego scooby-doo! blowout beach bash": ["000000", "bfbfbf"],  # LEGO Scooby-Doo! Blowout Beach Bash
            "lego scooby-doo!": ["000000", "bfbfbf"],  # LEGO Scooby-Doo!: Haunted Hollywood
            "lego": ["000000", "bfbfbf"],
            "lemony snicket's a series of unfortunate events": ["000000", "bfbfbf"],
            "little shop of horrors": ["000000", "bfbfbf"],  # Little Shop of Horrors
            "madagascar 2": ["000000", "bfbfbf"],
            "madagascar 3": ["000000", "bfbfbf"],
            "madagascar": ["000000", "bfbfbf"],
            "man of steel": ["000000", "bfbfbf"],
            "marley & me": ["000000", "bfbfbf"],
            "matilda": ["000000", "bfbfbf"],  # Matilda
            "megamind": ["000000", "bfbfbf"],
            "men in black 3": ["000000", "bfbfbf"],
            "men in black ii": ["000000", "bfbfbf"],
            "men in black": ["000000", "bfbfbf"],
            "minecon earth remix": ["000000", "bfbfbf"],
            "minions": ["000000", "bfbfbf"],
            "monsters vs. aliens": ["000000", "bfbfbf"],
            "mortal kombat": ["000000", "bfbfbf"],  # Mortal Kombat
            "movie": ["000000", "bfbfbf"],
            "mr. neighbor's house": ["000000", "bfbfbf"],
            "mr. popper's penguins": ["000000", "bfbfbf"],
            "nacho libre": ["000000", "bfbfbf"],
            "napoleon dynamite": ["000000", "bfbfbf"],
            "national lampoon's christmas vacation": ["000000", "bfbfbf"],  # National Lampoon's Christmas Vacation
            "nba all star slam dunk contest": ["000000", "bfbfbf"],  # NBA All Star Slam Dunk Contest
            "night at the museum": ["000000", "bfbfbf"],
            "ocean's eleven": ["000000", "bfbfbf"],  # Ocean's Eleven
            "ocean's thirteen": ["000000", "bfbfbf"],  # Ocean's Thirteen
            "ocean's twelve": ["000000", "bfbfbf"],  # Ocean's Twelve
            "over the hedge": ["000000", "bfbfbf"],
            "paddington 2": ["000000", "bfbfbf"],
            "paddington": ["000000", "bfbfbf"],
            "peter and the wolf": ["000000", "bfbfbf"],  # Peter and the Wolf
            "pirates of the caribbean": ["000000", "bfbfbf"],
            "pokémon detective pikachu": ["000000", "bfbfbf"],
            "poppy": ["000000", "bfbfbf"],
            "puberty": ["000000", "bfbfbf"],
            "puss in boots 2": ["000000", "bfbfbf"],
            "puss in boots": ["000000", "bfbfbf"],
            "r.l. stine's monsterville": ["000000", "bfbfbf"],
            "r.l. stine's mostly ghostly": ["000000", "bfbfbf"],
            "rampage": ["000000", "bfbfbf"],
            "ready player one": ["000000", "bfbfbf"],
            "regular show presents": ["ffffff", "582852"],  # Regular Show Presents: ...
            "relaxing old footage with joe pera": ["2d3866", "ece3df"],
            "rick and morty vs. genocider": ["00b2c8", "000000"],
            "rio 2": ["00b2c8", "000000"],
            "rio": ["00b2c8", "000000"],
            "rise of the guardians": ["000000", "bfbfbf"],
            "rogue one": ["000000", "bfbfbf"],
            "rv": ["000000", "bfbfbf"],  # RV
            "samurai and shogun": ["00b2c8", "000000"],
            "scoob! sneak peek": ["000000", "bfbfbf"],
            "scoob!": ["000000", "bfbfbf"],
            "scooby-doo! 2": ["000000", "bfbfbf"],
            "scooby-doo! abracadabra-doo!": ["000000", "bfbfbf"],  # Scooby-Doo! Abracadabra-Doo!
            "scooby-doo! and batman": ["000000", "bfbfbf"],
            "scooby-doo! and kiss": ["000000", "bfbfbf"],
            "scooby-doo! and the alien invaders": ["000000", "bfbfbf"],  # Scooby-Doo! and the Alien Invaders
            "scooby-doo! and the beach beastie": ["000000", "bfbfbf"],
            "scooby-doo! and the curse of the 13th ghost": ["000000", "bfbfbf"],
            "scooby-doo! and the cyber chase": ["000000", "bfbfbf"],
            "scooby-doo! and the ghoul school": ["000000", "bfbfbf"],  # Scooby-Doo! and the Ghoul School
            "scooby-doo! and the goblin king": ["000000", "bfbfbf"],
            "scooby-doo! and the gourmet ghost": ["000000", "bfbfbf"],  # Scooby-Doo! and The Gourmet Ghost
            "scooby-doo! and the legend of the vampire rock": ["000000", "bfbfbf"],
            "scooby-doo! and the legend of the vampire": ["000000", "bfbfbf"],
            "scooby-doo! and the loch ness monster": ["000000", "bfbfbf"],
            "scooby-doo! and the monster of mexico": ["000000", "bfbfbf"],
            "scooby-doo! and the samurai sword": ["000000", "bfbfbf"],
            "scooby-doo! and the witch's ghost": ["000000", "bfbfbf"],
            "scooby-doo! and wwe": ["000000", "bfbfbf"],
            "scooby-doo! camp scare": ["000000", "bfbfbf"],
            "scooby-doo! curse of the lake monster": ["000000", "bfbfbf"],
            "scooby-doo! haunted holidays": ["000000", "bfbfbf"],  # Scooby-Doo! Haunted Holidays
            "scooby-doo! in where's my mummy?": ["000000", "bfbfbf"],
            "scooby-doo! legend of the phantosaur": ["000000", "bfbfbf"],  # Scooby-Doo! Legend of the Phantosaur
            "scooby-doo! mask of the blue falcon": ["000000", "bfbfbf"],  # Scooby-Doo! Mask of the Blue Falcon
            "scooby-doo! meets the boo brothers": ["000000", "bfbfbf"],  # Scooby-Doo! Meets the Boo Brothers
            "scooby-doo! moon monster madness": ["000000", "bfbfbf"],
            "scooby-doo! music of the vampire": ["000000", "bfbfbf"],  # Scooby-Doo! Music of the Vampire
            "scooby-doo! on zombie island": ["000000", "bfbfbf"],
            "scooby-doo! pirates ahoy!": ["000000", "bfbfbf"],  # Scooby-Doo! Pirates Ahoy!
            "scooby-doo! return to zombie island": ["000000", "bfbfbf"],  # Scooby-Doo! Return to Zombie Island
            "scooby-doo! shaggy's showdown": ["000000", "bfbfbf"],
            "scooby-doo! stage fright": ["000000", "bfbfbf"],  # Scooby-Doo! Stage Fright
            "scooby-doo! the mystery begins": ["000000", "bfbfbf"],
            "scooby-doo! the sword and the scoob": ["000000", "bfbfbf"],  # Scooby-Doo! The Sword and the Scoob
            "scooby-doo! wrestlemania mystery": ["000000", "bfbfbf"],  # Scooby-Doo! WrestleMania Mystery
            "see us coming together": ["000000", "bfbfbf"],
            "shazam!": ["000000", "bfbfbf"],
            "she's the man": ["000000", "bfbfbf"],
            "sherlock gnomes": ["000000", "bfbfbf"],
            "shop": ["000000", "bfbfbf"],
            "shrek 2": ["000000", "bfbfbf"],
            "shrek forever after": ["000000", "bfbfbf"],
            "shrek": ["000000", "bfbfbf"],
            "smallfoot": ["000000", "bfbfbf"],
            "smalls-o-ween": ["000000", "bfbfbf"],
            "smalls": ["000000", "bfbfbf"],  # SMALLS
            "space jam": ["000000", "bfbfbf"],
            "special": ["000000", "bfbfbf"],
            "spider-man 2": ["000000", "bfbfbf"],
            "spider-man 3": ["000000", "bfbfbf"],
            "spider-man": ["000000", "bfbfbf"],
            "spy kids 2": ["000000", "bfbfbf"],
            "spy kids 3": ["000000", "bfbfbf"],
            "spy kids": ["000000", "bfbfbf"],
            "star wars": ["000000", "bfbfbf"],
            "steven universe the movie": ["000000", "bfbfbf"],
            "straight outta nowhere": ["000000", "bfbfbf"],  # Straight Outta Nowhere: Scooby-Doo! Meets Courage the Cowardly Dog
            "super mario bros.": ["000000", "bfbfbf"],  # Super Mario Bros.
            "superman ii": ["000000", "bfbfbf"],  # Superman II
            "superman iii": ["000000", "bfbfbf"],  # Superman III
            "superman iv": ["000000", "bfbfbf"],  # Superman IV: The Quest for Peace
            "superman": ["000000", "bfbfbf"],  # Superman
            "swords, knives, very sharp objects and cutlery": ["000000", "bfbfbf"],
            "teen titans go! & dc super hero girls": ["000000", "bfbfbf"],
            "teen titans go! beast boy's birthday": ["000000", "bfbfbf"],
            "teen titans go! in love": ["000000", "bfbfbf"],
            "teen titans go! nba slam dunk special": ["000000", "bfbfbf"],
            "teen titans go! see space jam": ["000000", "bfbfbf"],
            "teen titans go! titans vs. santa": ["000000", "bfbfbf"],
            "teen titans go! to the movies": ["000000", "bfbfbf"],
            "teen titans go! to work": ["000000", "bfbfbf"],
            "teen titans go! vs. teen titans real magic": ["000000", "bfbfbf"],
            "teen titans go! vs. teen titans": ["000000", "bfbfbf"],
            "teenage mutant ninja turtles ii": ["000000", "bfbfbf"],
            "teenage mutant ninja turtles iii": ["000000", "bfbfbf"],
            "teenage mutant ninja turtles": ["000000", "bfbfbf"],
            "the adult swim golf classic": ["000000", "bfbfbf"],
            "the amazing dates of gumball & penny": ["000000", "bfbfbf"],
            "the amazing spider-man": ["000000", "bfbfbf"],
            "the animatrix": ["000000", "bfbfbf"],
            "the book of life": ["000000", "bfbfbf"],
            "the croods": ["000000", "bfbfbf"],
            "the cube": ["000000", "bfbfbf"],
            "the elf on the shelf": ["000000", "bfbfbf"],
            "the emoji movie": ["000000", "bfbfbf"],  # The Emoji Movie
            "the goonies": ["000000", "bfbfbf"],  # The Goonies
            "the haunting hour": ["000000", "bfbfbf"],
            "the iron giant": ["000000", "bfbfbf"],
            "the jungle book": ["000000", "bfbfbf"],
            "the last open mic at the end of the world": ["000000", "bfbfbf"],
            "the lego 2": ["000000", "bfbfbf"],
            "the lego batman movie": ["000000", "bfbfbf"],
            "the lego justice league": ["000000", "bfbfbf"],
            "the lego movie 2": ["000000", "bfbfbf"],
            "the lego movie": ["000000", "bfbfbf"],
            "the lego ninjago movie": ["000000", "bfbfbf"],
            "the neverending story": ["000000", "bfbfbf"],  # The Neverending Story
            "the peanuts movie": ["000000", "bfbfbf"],  # The Peanuts Movie
            "the polar express": ["000000", "bfbfbf"],
            "the princess bride": ["000000", "bfbfbf"],  # The Princess Bride
            "the robot chicken walking dead special": ["000000", "bfbfbf"],  # The Robot Chicken Walking Dead Special: Look Who's Walking
            "the smurfs 2": ["000000", "bfbfbf"],  # The Smurfs 2
            "the smurfs": ["000000", "bfbfbf"],  # The Smurfs
            "the teen titans go! hollywood": ["000000", "bfbfbf"],
            "the wizard of oz": ["000000", "bfbfbf"],
            "the year without a santa claus": ["000000", "bfbfbf"],
            "tim burton's corpse bride": ["000000", "bfbfbf"],
            "tmnt": ["000000", "bfbfbf"],
            "tooth fairy": ["000000", "bfbfbf"],
            "trick or treat scooby-doo!": ["000000", "bfbfbf"],
            "twister": ["000000", "bfbfbf"],  # Twister
            "we bare bears the movie": ["000000", "bfbfbf"],
            "wet city": ["000000", "bfbfbf"],
            "who framed roger rabbit": ["000000", "bfbfbf"],  # Who Framed Roger Rabbit
            "willy wonka and the chocolate factory": ["000000", "bfbfbf"],  # Willy Wonka and the Chocolate Factory
            "wipeout": ["000000", "bfbfbf"],
            "wonder woman 1984": ["000000", "bfbfbf"],  # Wonder Woman 1984
            "wonder woman": ["000000", "bfbfbf"],
            "yogi bear": ["000000", "bfbfbf"],
            "young frankenstein": ["000000", "bfbfbf"],  # Young Frankenstein
            # todo
            "alpha bodies": ["000000", "00ceff"],
            "esme & roy": ["000000", "00ceff"],
            "fill": ["000000", "bfbfbf"],  # Fill
            "macbeth with dinosaurs": ["000000", "bfbfbf"],  # Macbeth with Dinosaurs
            "oh my god, yes!": ["000000", "bfbfbf"],  # Oh My God, Yes!
            "sesame street": ["000000", "bfbfbf"],  # Sesame Street
            "yenor": ["000000", "bfbfbf"],  # Yenor
            "todo": ["000000", "bfbfbf"],
        }

    def send_report(self):
        if len(self.reported) > 0:
            text = ""
            for t in self.reported:
                text += '            "'
                text += t["title"].replace('"', '\\"')
                text += '": ["000000", "bfbfbf"],  # '
                text += t["orig_title"] + "\n"

            em = Webhook(self.d_url, msg="```python\n" + text.strip() + "```")
            em.post()

    def get(self, title):
        orig_title = title
        title = title.lower()

        # Split at the colon
        title = title.split(":")[0]
        if title in self.theList:
            t = self.theList[title]

            return {
                "foreground": "#" + t[0],
                "background": "#" + t[1],
            }

        if not any(x["title"] == title for x in self.reported):
            self.reported.append({
                "orig_title": orig_title,
                "title": title,
            })

        return {
            "foreground": "#000000",
            "background": "#bfbfbf",
        }
