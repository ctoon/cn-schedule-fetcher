import re


def fix_showname(show_name):  # noqa: PLR0911
    # Remove (LWM) in titles
    show_name = show_name.replace(" (LWM)", "")

    # Remove years in parenthesis
    show_name = re.sub(r"( \(\d{4}\))$", "", show_name, 1)

    # Lower and split at the colon
    lname = show_name.lower().split(":")[0]

    if show_name == "MOVIE:":  # Remove the colon plz
        return "MOVIE"

    if show_name == "SPECIAL:":  # Remove the colon plz
        return "SPECIAL"

    if show_name in ("Cloudy", "Cloudy With a Chance of Meatballs"):  # Fix name
        return "Cloudy with a Chance of Meatballs"

    if show_name in ("The Amazing World of Gumball", "Gumball"):  # Fix name
        return "Amazing World of Gumball"

    if show_name in ("OK K.O.! Let's Be Heroes!", "OK K.O.!"):  # Fix name
        return "OK K.O.! Let's Be Heroes"

    if show_name == "Unikitty!":  # Fix name
        return "Unikitty"

    if show_name == "Superjail!":  # Fix name
        return "Superjail"

    if lname == "ninjago":  # Fix name
        return "LEGO Ninjago"

    if show_name == "LEGO DC Super Heroes: The Flash":  # Fix name
        return "LEGO DC Comics Super Heroes: The Flash"

    if show_name == "Total Dramarama":  # Fix name
        return "Total DramaRama"

    if show_name == "Spindeln Lucas":  # Fix name (not translated)
        return "Lucas the Spider"

    if show_name in ("The Grim Adventures of Billy & Mandy", "Billy and Mandy"):  # Fix name
        return "The Grim Adventures of Billy and Mandy"

    if lname == "ed, edd 'n eddy's jingle jingle jangle":  # Fix name
        return "Ed, Edd n Eddy's Jingle, Jingle, Jangle"

    if show_name == "Ed, Edd 'n Eddy":  # Fix name
        return "Ed, Edd n Eddy"

    if lname == "sylvester & tweety mysteries":  # Fix name
        return "The Sylvester & Tweety Mysteries"

    if show_name == "Thomas and Friends: All Engines Go":  # Fix name
        return "Thomas & Friends: All Engines Go"

    if show_name == "Primal":  # Fix name
        return "Genndy Tartakovsky's Primal"

    if show_name == "The Venture Bros.":  # Fix name
        return "The Venture Brothers"

    if lname == "teen titans go":  # Make sure it has an exclamation mark
        return "Teen Titans Go!"

    if lname == "sesame street mecha builders":  # Fix name
        return "Mecha Builders"

    if lname == "foster's home":  # Fix name
        return "Foster's Home for Imaginary Friends"

    if lname == "flapjack":  # Fix name
        return "The Marvelous Misadventures of Flapjack"

    if lname == "adventure time stakes":  # Fix name
        return "Adventure Time: Stakes!"

    if lname == "adventure time - abenteuerzeit mit finn und jake":  # Wrong language
        return "Adventure Time"

    if lname == "batwheels":  # Fix name
        return "Meet the Batwheels"

    if lname in ("nba all star slam", "nba all-star slam"):  # Fix name
        return "NBA All Star Slam Dunk Contest"

    if lname == "american dad":  # Make sure it has an exclamation mark
        return "American Dad!"

    if lname == "adult swim yule log (aka the fireplace)":  # Remove text in parenthesis
        return "Adult Swim Yule Log"

    if lname == "batman returns (linear)":  # Remove text in parenthesis
        return "Batman Returns"

    if "eric andré" in lname:  # Fix name
        return show_name.replace("Eric André", "Eric Andre").strip()

    if lname in ("mostly ghostly", "monsterville"):  # Fix name
        return "R.L. Stine's " + show_name

    if "r.l stine" in lname:  # Fix name
        return show_name.replace("R.L Stine", "R.L. Stine").strip()

    if lname in ("tom & jerry kids show", "tom and jerry kids show"):  # Fix name
        return "Tom & Jerry Kids"

    if lname in ("the tom & jerry show", "the tom and jerry show"):  # Fix name
        return "Tom & Jerry Show"

    if "tom and jerry" in lname:  # Fix name
        return show_name.replace("Tom and Jerry", "Tom & Jerry").replace("Tom And Jerry", "Tom & Jerry").strip()

    if lname == "scooby-doo! the sword and the scoob!":  # Fix name (don't use the exclamation mark here)
        return "Scooby-Doo! The Sword and the Scoob"

    if lname in ("what's new scooby doo", "what's new scooby doo!", "what's new scooby doo?",
                 "what's new scooby-doo", "what's new scooby-doo!", "what's new scooby-doo?"):  # Fix or keep name
        return "What's New Scooby-Doo?"

    if lname in ("scooby doo where are you!", "scooby doo where are you?",
                 "scooby-doo where are you!", "scooby-doo where are you?",
                 "scooby doo, where are you!", "scooby doo, where are you?",
                 "scooby-doo, where are you!", "scooby-doo, where are you?"):  # Fix or keep name
        return "Scooby-Doo, Where Are You!"

    if lname == "scooby-doo! trick or treat":  # Fix name
        return "Trick or Treat Scooby-Doo!"

    if lname == "scooby-doo! pirates ahoy":  # Fix name
        return "Scooby-Doo! Pirates Ahoy!"

    if lname == "scooby-doo and guess who":  # Fix name
        return "Scooby-Doo! and Guess Who?"

    if lname == "scooby-doo! where's my mummy?":  # Fix name
        return "Scooby-Doo! in Where's My Mummy?"

    if lname == "scooby-doo! the wwe mystery":  # Fix name
        return "Scooby-Doo! WrestleMania Mystery"

    if lname == "scooby-doo! mystery incorporated":  # Fix name
        return "Scooby-Doo! Mystery Inc"

    if lname == "the new scooby-doo mysteries":  # Keep name without excalamation mark
        return "The New Scooby-Doo Mysteries"

    if "Scooby Doo &" in show_name:  # Fix name + make sure it has an exclamation mark
        return show_name.replace("Scooby Doo &", "Scooby-Doo! and")

    if "Scooby Doo! &" in show_name:  # Fix name
        return show_name.replace("Scooby Doo! &", "Scooby-Doo! and")

    if "Scooby-Doo &" in show_name:  # Fix name + make sure it has an exclamation mark
        return show_name.replace("Scooby-Doo &", "Scooby-Doo! and")

    if "Scooby-Doo! &" in show_name:  # Fix name
        return show_name.replace("Scooby-Doo! &", "Scooby-Doo! and")

    if "Scooby Doo" in show_name:  # Fix name + make sure it has an exclamation mark (but not two)
        return show_name.replace("Scooby Doo", "Scooby-Doo!").replace("Doo!!", "Doo!").strip()

    if "Scooby-Doo" in show_name:  # Make sure it has an exclamation mark (but not two)
        return show_name.replace("Doo", "Doo!").replace("Doo!!", "Doo!").strip()

    return show_name.strip()
