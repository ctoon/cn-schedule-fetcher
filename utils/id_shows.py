from time import sleep

from .discord_hooks import Webhook


class IdShows:
    def __init__(self, config):
        self.d_url = config["discord"]["issues"]
        self.reported = []

        self.theList = {
            "399692": "Steven Universe",
            "433712": "Justice League Action",
            "326561": "Teen Titans",
            "393474": "Teen Titans Go!",
            "376453": "Amazing World of Gumball",
            "438472": "Ben 10",
            "327186": "MOVIE",
            "446892": "Craig of the Creek",
            "442472": "Unikitty",
            "324534": "SPECIAL",
            "419032": "We Bare Bears",
            "440832": "OK K.O.! Let's Be Heroes",
            "381332": "NINJAGO",
            "423572": "Mighty Magiswords",
            "425772": "The Powerpuff Girls",
            "444772": "Apple & Onion",
            "403152": "Clarence",
            "368594": "Adventure Time",
            "449432": "Summer Camp Island",
            "450052": "Mega Man",
            "450712": "Total DramaRama",
            "450713": "Transformers: Cybertron",
            "424292": "Bunnicula",
            "325516": "Total Drama Island",
            "453213": "Dorothy and the Wizard of Oz",
            "366212": "Bakugan: Battle Planet",
        }

    def get(self, id):
        # Make sure it's always a string
        id = str(id)

        if id in self.theList:
            return self.theList[id]

        if id not in self.reported:
            em = Webhook(self.d_url, msg="Missing show: `'" + id + "'`")
            em.post()
            self.reported.append(id)
            sleep(2)

        return "??? (Show ID: " + id + ")"
