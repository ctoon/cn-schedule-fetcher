# ruff: noqa
# This file contains all our older fetchers, saved here for reference
# We reimport stuff here just to not get too much red in the IDE
import better_exceptions
import collections
import json
import math
import os
import re
import requests
import timeout_decorator
import traceback

from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta
from dateutil import parser, tz
from discord_hooks import Webhook
from lxml import etree
from operator import itemgetter
from time import sleep

from fetch import log, pretty, upload_schedule, parse_id, rheaders, et, config
from fix_showname import fix_showname

better_exceptions.MAX_LENGTH = None
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)

as_has_5pm_starting = 1693202400  # August 28th 2023 - 6am UTC
as_has_7pm_starting = 1682920800  # May 1st 2023 - 6am UTC
cn_has_8pm_starting = 1664172000  # Sep 26 2022 - 6am UTC
cn_has_8pm_ending = 1672124400  # Dec 27 2022 - 6am UTC
cn_has_8pm_wd_starting = 0  # 0 = Monday, 6 = Sunday
cn_has_8pm_wd_ending = 5  # Including


def fix_episodename(episode_name, force_the=False):
    # Space at the end of titles means there's actually a ", The"
    # Add the "The" back at the beginning and remove the trailing space
    # Also fix when title ends with ", A" and ", An"
    for title in episode_name.split('/'):
        fixed = re.sub(r'(.*?) $', 'The \\1', title)
        # We might want to force it, like with Gumball episodes
        # Else, we just continue with the The/A/An check
        if force_the and not fixed.startswith('The '):
            fixed = 'The ' + fixed
        else:
            fixed = re.sub(r'(.*?), The$', 'The \\1', fixed)
            fixed = re.sub(r'(.*?), An$', 'An \\1', fixed)
            fixed = re.sub(r'(.*?), A$', 'A \\1', fixed)

        episode_name = episode_name.replace(title, fixed)

    return episode_name


@timeout_decorator.timeout(300)
def grab_cnschedule(url):
    # Get the schedule from the "backdoor" and parse it for every available day
    global colors, cn_last
    schedule = {}
    available_dates = []

    log('cn', 'Will get today\'s page')

    # Get the file for today and let BS parse it
    d = date.today()
    now = '%02d/%02d/%d' % (d.month, d.day, d.year)
    r = requests.get(url % now)

    log('cn', 'Parsing...')

    soup = BeautifulSoup(r.text, 'html5lib')

    # We'll now save all the available dates
    try:
        for select_date in soup.find_all('select')[1].find_all('option'):
            if select_date['value'] != '0':
                available_dates.append(select_date['value'])
    except:
        return schedule

    log('cn', 'Parsed list of days.')

    # Now, we'll go throught each day
    for available_date in available_dates:
        # Get a YYYY-MM-DD version of the date
        parsed_date = parser.parse(available_date).strftime('%Y-%m-%d')
        # Init the list and the increment counter
        schedule[parsed_date] = {
            'date': parsed_date,
            'source': 'Cartoon Network',
            'schedule': []
        }
        i = 0

        log('cn', 'Downloading and parsing ' + parsed_date)

        # Get the day's page
        r = requests.get(url % available_date)
        soup = BeautifulSoup(r.text, 'html5lib')

        log('cn', 'Parsed. Extracting data...')

        # And let's go directly to where the list is (yup, it's a bit complicated)
        trs = soup.find_all('table')[2].find('tr').find_all('td', recursive=False)[1]\
            .find_all('table', recursive=False)[1].find_all('td')[3]\
            .find_all('table', recursive=False)[2].find('tbody').find_all('tr', attrs={'valign': 'top'})

        # And now, for each lines...
        for i, tr in enumerate(trs):
            alt_table = False
            el_time = tr.find(class_='timecell')  # Get the element that contains the time

            if el_time is None:
                el_time = tr.find(class_='timeoncell')
                alt_table = True

            if not pretty(''.join(el_time.strings)).endswith('m'):
                continue

            try:
                # Get the "time" and "show" details
                if alt_table:  # Sometimes, it's using '[...]oncell' instead of just '[...]cell'
                    el_time = tr.find(class_='timeoncell')   # Get the element that contains the time
                    el_show = tr.find_all(class_='showoncell')  # Get the element**s** that contains show details
                    el_show_name = el_show[0].find('a')  # Get the element that contains the show name
                    show_name = pretty(''.join(el_show_name.strings))  # Get the actual show name
                    episode_name = pretty(el_show[2].string)  # Get the episode title

                    # Show might not be under a link, so we try to get the first string in the element then
                    if show_name == 'See All Showings':
                        show_name = pretty(el_show[0].contents[0])

                    # Episode name might be under a link
                    if episode_name is None:
                        el_episode_name = el_show[2].find('a')
                        episode_name = pretty(''.join(el_episode_name.strings))

                else:  # If it's not, let's get our values
                    el_show = tr.find_all(class_='showcell')  # Get the element**s** that contains show details
                    el_show_name = el_show[1].find('a')  # Get the element that contains the show name
                    show_name = pretty(''.join(el_show_name.strings))  # Get the actual show name
                    episode_name = el_show[3].string  # Get the episode title

                    # Show might not be under a link, so we try to get the first string in the element then
                    if show_name == 'See All Showings':
                        show_name = pretty(el_show[1].contents[0])

                    # Episode name might be under a link
                    try:
                        if episode_name is None:
                            el_episode_name = el_show[3].find('a')
                            episode_name = pretty(''.join(el_episode_name.strings))
                    except:
                        pass

                show_name = fix_showname(show_name)

                if episode_name is None:  # If no episode title is found, return an empty string
                    episode_name = ''

                episode_name = fix_episodename(episode_name, (show_name == 'Amazing World of Gumball'))

                try:
                    el_next_time = trs[i + 2].find(class_='timecell')
                    if el_next_time is None:
                        el_next_time = tr.find(class_='timeoncell')   # Get the element that contains the time

                    next_time = pretty(''.join(el_next_time.strings))
                except IndexError:
                    next_time = '6:00 am'

                time = pretty(''.join(el_time.strings))  # Get the air time
                tzinfos = {'US': tz.gettz('America/New York')}
                parsed_datetime = parser.parse('%s %s US' % (parsed_date, time), tzinfos=tzinfos)  # And let's parse it
                parsed_next_datetime = parser.parse('%s %s US' % (parsed_date, next_time), tzinfos=tzinfos)  # And let's parse it

                # If it's before 6am and after (including) 8pm, it's the adultswim part
                if parsed_datetime.hour < 6 or parsed_datetime.hour >= 20:
                    continue

                start_ts = int(parsed_datetime.timestamp())
                end_ts = int(parsed_next_datetime.timestamp())
                if end_ts < start_ts:  # if it broke, make it 30 minutes
                    end_ts = start_ts + 1800
                length_minutes = math.ceil((end_ts - start_ts) / 60)

                slots = math.ceil(length_minutes / 15)
                if slots < 1:
                    slots = 1

                # Add all the details to our schedule list
                schedule[parsed_date]['schedule'].append({
                    'time': time,
                    'date': parsed_date,
                    'show': show_name,
                    'title': episode_name,
                    'colors': colors.get(show_name),
                    'timestamp': start_ts,
                    'timestamp_end': end_ts,
                    'minutes': length_minutes,
                    'slots': slots
                })

                # Save last date
                cn_last = {
                    'ts': end_ts,
                    'date': parsed_date
                }
            except:
                # If something goes wrong, show the <tr> and raise the error
                print(tr)
                raise

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log('cn', 'Uploading ' + parsed_date + '...')
        upload_schedule(schedule[parsed_date])

    log('cn', 'Done!')
    return schedule


@timeout_decorator.timeout(300)
def grab_cnxml(url):
    # Get the schedule from the XMLs
    global colors, cn_last, trash
    schedule = {}

    # Starts from a week ago and go up until dates in XML don't match
    d = date.today() - timedelta(days=7)

    while True:
        # Get a YYYY-MM-DD version of the date
        parsed_date = d.strftime('%Y-%m-%d')

        log('cn', 'Parsing ' + parsed_date + '...')

        # Get the day's page
        r = requests.get(url % d.day)
        soup = BeautifulSoup(r.text, 'lxml')

        log('cn', 'Parsed. Extracting data...')

        # And let's load up directly the shows
        lines = soup.findAll('show')

        # Check if correct date
        if lines[0]['date'] != d.strftime('%m/%d/%Y'):
            break

        # Init the list and the increment counter
        schedule[parsed_date] = {
            'date': parsed_date,
            'source': 'Cartoon Network',
            'schedule': []
        }

        # And now, for each lines...
        for i, line in enumerate(lines):
            try:
                # Ignore if it's an [as] program
                if line['blockname'] == 'AdultSwim':
                    continue

                show_name = fix_showname(line['title'])
                episode_name = line['episodename']

                if episode_name is None:  # If no episode title is found, return an empty string
                    episode_name = ''

                # In case of broken schedule
                if show_name == 'Cartoon Network':
                    show_name = fix_showname(parse_id.get(line['showid']))
                    episode_name = ''

                    try:
                        # Use getEpisodeDesc method to find the title
                        log('cn', 'Parsed. Extracting data... (Fetching descriptions)')
                        t_url = 'https://www.adultswim.com/adultswimdynsched/xmlServices/ScheduleServices?' +\
                                'methodName=getEpisodeDesc&showId=%s&episodeId=%s&isFeatured=%s'
                        t_r = requests.get(t_url % (line['showid'], line['episodeid'], line['isfeatured']))
                        # BeautifulSoup doesn't help here for some reason
                        t_desc = etree.XML(t_r.content).xpath('//Desc/episodeDesc/text()')[0]
                        t_desc = t_desc.split('<br>')[0]  # There may be a synopsis in there
                        episode_name = t_desc.strip()
                    except Exception as e:
                        trash = e
                        pass

                # Apply fixes for common weird title format
                episode_name = fix_episodename(episode_name)

                time = line['time']  # Get the air time
                time = time.replace('.', '')
                tzinfos = {'US': tz.gettz('America/New York')}
                parsed_datetime = parser.parse('%s %s US' % (parsed_date, time), tzinfos=tzinfos)  # And let's parse it
                try:
                    parsed_next_datetime = parser.parse('%s %s US' % (parsed_date, lines[i + 1]['time']), tzinfos=tzinfos)
                except IndexError:
                    next_day = d + timedelta(days=1)
                    parsed_next_datetime = parser.parse('%s 00:00 US' % next_day.strftime('%Y-%m-%d'), tzinfos=tzinfos)

                start_ts = int(parsed_datetime.timestamp())
                end_ts = int(parsed_next_datetime.timestamp())
                if end_ts < start_ts:  # if it broke, make it 30 minutes
                    end_ts = start_ts + 1800
                length_minutes = math.ceil((end_ts - start_ts) / 60)

                slots = math.ceil(length_minutes / 15)
                if slots < 1:
                    slots = 1

                # Add all the details to our schedule list
                schedule[parsed_date]['schedule'].append({
                    'time': time,
                    'date': parsed_date,
                    'show': show_name,
                    'title': episode_name,
                    'colors': colors.get(show_name),
                    'timestamp': start_ts,
                    'timestamp_end': end_ts,
                    'minutes': length_minutes,
                    'slots': slots
                })

                # Save last date
                cn_last = {
                    'ts': end_ts,
                    'date': parsed_date
                }
            except:
                # If something goes wrong, show the line and raise the error
                print(line)
                raise

        # Go to next day
        d = d + timedelta(days=1)

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log('cn', 'Uploading ' + parsed_date + '...')
        upload_schedule(schedule[parsed_date])

    log('cn', 'Done!')
    return schedule


@timeout_decorator.timeout(300)
def grab_asxml(url):
    # Get the schedule from the XMLs
    global colors, as_last, trash
    schedule = {}

    # Starts with yesterday and go up until dates in XML don't match
    d = date.today() - timedelta(days=1)

    while True:
        # Get a YYYY-MM-DD version of the date
        parsed_date = d.strftime('%Y-%m-%d')

        log('as', 'Parsing ' + parsed_date + '...')

        # Get the day's page
        r = requests.get(url % d.day)
        soup = BeautifulSoup(r.text, 'lxml')

        log('as', 'Parsed. Extracting data...')

        # And let's load up directly the shows
        lines = soup.findAll('show')

        # Check if correct date
        if lines[0]['date'] != d.strftime('%m/%d/%Y'):
            break

        # Init the list and the increment counter
        schedule[parsed_date] = {
            'date': parsed_date,
            'source': 'Adult Swim',
            'schedule': []
        }

        # And now, for each lines...
        for i, line in enumerate(lines):
            try:
                show_name = fix_showname(line['title'])
                episode_name = line['episodename']

                if episode_name is None:  # If no episode title is found, return an empty string
                    episode_name = ''

                # In case of broken schedule
                if show_name == 'Adult Swim':
                    show_name = fix_showname(parse_id.get(line['showid']))
                    episode_name = ''

                    try:
                        # Use getEpisodeDesc method to find the title
                        log('cn', 'Parsed. Extracting data... (Fetching descriptions)')
                        t_url = 'https://www.adultswim.com/adultswimdynsched/xmlServices/ScheduleServices?' +\
                                'methodName=getEpisodeDesc&showId=%s&episodeId=%s&isFeatured=%s'
                        t_r = requests.get(t_url % (line['showid'], line['episodeid'], line['isfeatured']))
                        # BeautifulSoup doesn't help here for some reason
                        t_desc = etree.XML(t_r.content).xpath('//Desc/episodeDesc/text()')[0]
                        t_desc = t_desc.split('<br>')[0]  # There may be a synopsis in there
                        episode_name = t_desc.strip()
                    except Exception as e:
                        trash = e
                        pass

                # Apply fixes for common weird title format
                episode_name = fix_episodename(episode_name)

                time = line['time']  # Get the air time
                time = time.replace('.', '')
                tzinfos = {'US': tz.gettz('America/New York')}
                parsed_datetime = parser.parse('%s %s US' % (line['date'], time), tzinfos=tzinfos)  # And let's parse it
                try:
                    parsed_next_datetime = parser.parse('%s %s US' % (line['date'], lines[i + 1]['time']), tzinfos=tzinfos)
                except IndexError:
                    next_day = d + timedelta(days=1)
                    parsed_next_datetime = parser.parse('%s 00:00 US' % next_day.strftime('%Y-%m-%d'), tzinfos=tzinfos)

                start_ts = int(parsed_datetime.timestamp())
                end_ts = int(parsed_next_datetime.timestamp())
                if end_ts < start_ts:  # if it broke, make it 30 minutes
                    end_ts = start_ts + 1800
                length_minutes = math.ceil((end_ts - start_ts) / 60)

                slots = math.ceil(length_minutes / 15)
                if slots < 1:
                    slots = 1

                # Add all the details to our schedule list
                schedule[parsed_date]['schedule'].append({
                    'time': time,
                    'date': parsed_datetime.strftime('%Y-%m-%d'),
                    'show': show_name,
                    'title': episode_name,
                    'colors': colors.get(show_name),
                    'timestamp': start_ts,
                    'timestamp_end': end_ts,
                    'minutes': length_minutes,
                    'slots': slots
                })

                # Save last date
                as_last = {
                    'ts': end_ts,
                    'date': parsed_date
                }
            except:
                # If something goes wrong, show the line and raise the error
                print(line)
                raise

        # Go to next day
        d = d + timedelta(days=1)

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log('as', 'Uploading ' + parsed_date + '...')
        upload_schedule(schedule[parsed_date])

    log('as', 'Done!')
    return schedule


@timeout_decorator.timeout(300)
def grab_asjson(url):
    # Get the schedule from the "hidden" API
    global colors, as_last
    json = None
    schedule = {}

    log('as', 'Fetching API')
    try:
        r = requests.get(url, headers=rheaders)

        # In case we break
        if r.status_code == 502:
            print('\n 502 when fetching AS, waiting...')
            sleep(30)
            return schedule

        json = r.json()
    except Exception as e:
        print('\n Exception when fetching AS, skipping...')
        print(e)
        sleep(5)
        return schedule

    for i, slot in enumerate(json['data']):
        # Get a YYYY-MM-DD version of the date
        parsed_date = parser.parse(slot['date']).strftime('%Y-%m-%d')

        show_name = fix_showname(slot['showTitle'])
        episode_name = slot['episodeTitle']
        start_datetime = parser.parse(slot['datetime'])

        if show_name == 'Family Weekend Movies':
            continue

        # When [as] is not taking 5pm, it should start at 7pm, ignore 5-6pm
        if (start_datetime.timestamp() < as_has_5pm_starting and
            start_datetime.timestamp() > cn_has_8pm_starting and
            start_datetime.timestamp() < cn_has_8pm_ending and
            start_datetime.weekday() >= cn_has_8pm_wd_starting and
            start_datetime.weekday() <= cn_has_8pm_ending and (
                start_datetime.strftime('%H') == '17' or
                start_datetime.strftime('%H') == '18')):
            continue
        elif int(start_datetime.strftime('%H')) < 17 and int(start_datetime.strftime('%H')) > 6:
            # When CN content goes over...
            continue

        try:
            if slot['date'] == json['data'][i + 1]['date']:
                end_datetime = parser.parse(json['data'][i + 1]['datetime'])
            else:
                end_datetime = start_datetime.replace(hour=6, minute=0)
        except IndexError:
            end_datetime = start_datetime.replace(hour=6, minute=0)

        # Get length
        length_minutes = math.ceil((end_datetime - start_datetime).seconds / 60)

        slots = math.ceil(length_minutes / 15)
        if slots < 1:
            slots = 1

        # Init the list and the increment counter
        if parsed_date not in schedule:
            schedule[parsed_date] = {
                'date': parsed_date,
                'source': 'Adult Swim',
                'schedule': []
            }

        # Add all the details to our schedule list
        schedule[parsed_date]['schedule'].append({
            'time': start_datetime.strftime('%-I:%M %p').lower(),
            'date': parsed_date,
            'show': show_name,
            'title': episode_name,
            'colors': colors.get(show_name),
            'timestamp': start_datetime.timestamp(),
            'timestamp_end': end_datetime.timestamp(),
            'minutes': length_minutes,
            'slots': slots
        })

        # Save date
        as_last = {
            'ts': int(end_datetime.timestamp()),
            'date': parsed_date
        }

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log('as', 'Uploading ' + parsed_date + '...')
        upload_schedule(schedule[parsed_date])

    log('as', 'Done!')
    return schedule


@timeout_decorator.timeout(300)
def grab_ashtml():
    # Get the schedule from the schedule page
    # NOTE: I am aware that there's a neat JSON left by NextJS in there,
    # but I think it would be a mess to use that (and might break anytime)
    global colors, as_last
    schedule = {}

    log('as', 'Fetching page')
    try:
        url = 'https://www.adultswim.com/schedule/'
        r = requests.get(url, headers=rheaders)

        # In case we break
        if r.status_code == 502:
            print('\n 502 when fetching AS, waiting...')
            sleep(30)
            return schedule

        log('as', 'Parsing...')

        soup = BeautifulSoup(r.text, 'html5lib')
    except Exception as e:
        print('\n Exception when fetching AS, skipping...')
        print(e)
        sleep(5)
        return schedule

    trs = soup.select('tr[itemType="https://schema.org/TVEpisode"]')

    for i, tr in enumerate(trs):
        # Grab our elements
        el_time = tr.select('time[datetime]')[0]
        el_series = tr.select('td[data-col="series"] span')[0]
        el_episode = tr.select('td[data-col="episode"]')[0]

        # Get a YYYY-MM-DD version of the date
        start_datetime_utc = parser.parse(el_time['datetime'])
        # Fix timezone (this is fucked up and I don't get the same result as the server but oh well)
        start_datetime_utc = start_datetime_utc.replace(tzinfo=tz.gettz('UTC'))
        start_datetime = start_datetime_utc.astimezone(tz.gettz('America/New York'))
        parsed_date_orig = start_datetime.strftime('%Y-%m-%d')

        if int(start_datetime.strftime('%H')) <= 6:
            # Before 6am, consider as the previous day
            previous_day = start_datetime - timedelta(days=1)
            parsed_date = previous_day.strftime('%Y-%m-%d')
        else:
            parsed_date = parsed_date_orig

        show_name = fix_showname(pretty(''.join(el_series.strings)))
        episode_name = pretty(''.join(el_episode.strings))

        if show_name == 'Family Weekend Movies':
            continue

        # When [as] is not taking 5pm, it should start at 7pm, ignore 5-6pm
        if (start_datetime.timestamp() < as_has_5pm_starting and
            start_datetime.timestamp() > cn_has_8pm_starting and
            start_datetime.timestamp() < cn_has_8pm_ending and
            start_datetime.weekday() >= cn_has_8pm_wd_starting and
            start_datetime.weekday() <= cn_has_8pm_ending and (
                start_datetime.strftime('%H') == '17' or
                start_datetime.strftime('%H') == '18')):
            continue
        elif int(start_datetime.strftime('%H')) < 17 and int(start_datetime.strftime('%H')) > 6:
            # When CN content goes over...
            continue

        try:
            next_datetime_utc = parser.parse(trs[i + 1].select('time[datetime]')[0]['datetime'])
            next_datetime_utc = next_datetime_utc.replace(tzinfo=tz.gettz('UTC'))
            next_datetime = next_datetime_utc.astimezone(tz.gettz('America/New York'))
            if int(next_datetime.strftime('%H')) <= 6:
                # Before 6am, consider as the previous day
                next_previous_day = next_datetime - timedelta(days=1)
                next_date = next_previous_day.strftime('%Y-%m-%d')
            else:
                next_date = next_datetime.strftime('%Y-%m-%d')

            if parsed_date == next_date:
                end_datetime = next_datetime
            else:
                end_datetime = start_datetime.replace(hour=6, minute=0)
        except IndexError:
            end_datetime = start_datetime.replace(hour=6, minute=0)

        # Get length
        length_minutes = math.ceil((end_datetime - start_datetime).seconds / 60)

        slots = math.ceil(length_minutes / 15)
        if slots < 1:
            slots = 1

        # Init the list and the increment counter
        if parsed_date not in schedule:
            schedule[parsed_date] = {
                'date': parsed_date,
                'source': 'Adult Swim',
                'schedule': []
            }

        # Add all the details to our schedule list
        schedule[parsed_date]['schedule'].append({
            'time': start_datetime.strftime('%-I:%M %p').lower(),
            'date': parsed_date_orig,
            'show': show_name,
            'title': episode_name,
            'colors': colors.get(show_name),
            'timestamp': start_datetime.timestamp(),
            'timestamp_end': end_datetime.timestamp(),
            'minutes': length_minutes,
            'slots': slots
        })

        # Save date
        as_last = {
            'ts': int(end_datetime.timestamp()),
            'date': parsed_date
        }

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log('as', 'Uploading ' + parsed_date + '...')
        upload_schedule(schedule[parsed_date])

    log('as', 'Done!')
    return schedule


@timeout_decorator.timeout(300)
def grab_ngtv(url):
    # Get the schedule from the "hidden" API
    global colors, ngtv_last
    json = None
    schedule = {}

    log('ngtv', 'Fetching API')
    try:
        r = requests.get(url, headers=rheaders)

        # In case we break
        if r.status_code == 502:
            print('\n 502 when fetching NGTV, waiting...')
            sleep(30)
            return schedule

        json = r.json()
    except Exception as e:
        print('\n Exception when fetching NGTV, skipping...')
        print(e)
        sleep(5)
        return schedule

    for i, slot in enumerate(json['shows']):
        # Get a YYYY-MM-DD version of the date
        parsed_date = datetime.fromtimestamp(slot['scheduled_timestamp']).astimezone(et).strftime('%Y-%m-%d')

        show_name = fix_showname(slot['franchise_name'])
        episode_name = slot['title']
        start_ts = slot['scheduled_timestamp']

        try:
            end_ts = json['shows'][i + 1]['scheduled_timestamp']
        except IndexError:
            end_ts = slot['scheduled_timestamp'] + slot['scheduled_duration']

        # Parse UTC date
        start_datetime = datetime.fromtimestamp(start_ts)
        end_datetime = datetime.fromtimestamp(end_ts)

        # to ET
        start_et_datetime = start_datetime.astimezone(et)
        end_et_datetime = end_datetime.astimezone(et)

        # Get length
        length_minutes = math.ceil((end_et_datetime - start_et_datetime).seconds / 60)

        # If it's not between 6am and 5pm (exclusive) (ET) then ignore it's [Adult Swim]
        max_hour = 17
        # Before [as] taking 7pm
        if (start_ts < as_has_7pm_starting):
            max_hour = 20
        # Before [as] taking 5pm
        elif (start_ts < as_has_5pm_starting):
            max_hour = 19
        # If CN has 8pm
        elif (start_ts > cn_has_8pm_starting and
              start_ts < cn_has_8pm_ending and
              start_et_datetime.weekday() >= cn_has_8pm_wd_starting and
              start_et_datetime.weekday() <= cn_has_8pm_ending):
            max_hour = 21

        slots = math.ceil(length_minutes / 15)
        if slots < 1:
            slots = 1

        if start_et_datetime.hour >= 6 and start_et_datetime.hour < max_hour:
            # Init the list and the increment counter
            if parsed_date not in schedule:
                schedule[parsed_date] = {
                    'date': parsed_date,
                    'source': 'ngtv-cn',
                    'schedule': []
                }

            # Add all the details to our schedule list
            schedule[parsed_date]['schedule'].append({
                'time': start_et_datetime.strftime('%-I:%M %p').lower(),
                'date': parsed_date,
                'show': show_name,
                'title': episode_name,
                'colors': colors.get(show_name),
                'timestamp': start_ts,
                'timestamp_end': end_ts,
                'minutes': length_minutes,
                'slots': slots
            })

            # Save date if ends at least at 8pm ET (before May 1st '23), 5pm (after August 28th '23) or 7pm (after May 1st '23)
            if ((start_ts < as_has_7pm_starting and end_et_datetime.hour >= 20) or
                (start_ts > as_has_5pm_starting and end_et_datetime.hour >= 17) or
                (start_ts > as_has_7pm_starting and end_et_datetime.hour >= 19)):
                ngtv_last = {
                    'ts': end_ts,
                    'date': parsed_date
                }

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log('ngtv', 'Uploading ' + parsed_date + '...')
        upload_schedule(schedule[parsed_date])

    log('ngtv', 'Done!')
    return schedule


def merge_schedules(scheds):
    sch = {}
    if 'cn' not in scheds:
        log('merge', '/!\\ No CN, skipping...')
        return {}

    sch = scheds['cn']

    if 'as' in scheds:
        log('merge', 'Adding [adult swim]...')
        for s_date, __ in scheds['as'].items():
            # Just append
            try:
                if s_date in sch:
                    sch[s_date]['schedule'] += scheds['as'][s_date]['schedule']
            except:
                pass

    if 'zap' in scheds or 'tvguide' in scheds:
        log('merge', 'Fixing empty schedules...')
        for s_date, values in sch.items():
            try:
                if len(values['schedule']) == 0:
                    if 'zap' in scheds:
                        sch[s_date] = scheds['zap'][s_date]
                    else:
                        sch[s_date] = scheds['tvguide'][s_date]
            except:
                pass

    if 'tvguide' in scheds:
        log('merge', 'Adding TVGuide...')
        for s_date, __ in scheds['tvguide'].items():
            try:
                if s_date in sch:
                    sch[s_date]['alt'] = scheds['tvguide'][s_date]
                else:
                    sch[s_date] = scheds['tvguide'][s_date]
            except:
                pass

    if 'zap' in scheds:
        log('merge', 'Adding Zap2it...')
        for s_date, __ in scheds['zap'].items():
            try:
                if 'tvguide' in scheds and s_date in scheds['cn'] and s_date not in scheds['tvguide']:
                    sch[s_date]['alt'] = scheds['zap'][s_date]
                elif 'tvguide' not in scheds and s_date in sch:
                    sch[s_date]['alt'] = scheds['zap'][s_date]
                elif s_date not in sch:
                    sch[s_date] = scheds['zap'][s_date]
            except:
                pass

    log('merge', 'Ordering...')
    for day in sch:
        sch[day]['schedule'] = sorted(sch[day]['schedule'], key=itemgetter('timestamp'))

    ordered = collections.OrderedDict(sorted(sch.items()))

    log('merge', 'Done!')
    return ordered


def gen_file(sch):
    # Save our schedule in a json file
    log('json', 'Generating json...')

    sch['_'] = {'generated': int(datetime.utcnow().timestamp())}

    with open(os.path.join(THIS_DIR, 'all.json'), 'w') as f:
        f.write(json.dumps(sch, indent=2, sort_keys=False))

    log('json', 'Done!')


def never_execute_me_i_have_old_stuff_that_doesnt_work():
    cn_schedule = {}
    as_schedule = {}

    # Using the [as] "backdoor", where we can put a date in the URL
    source_url = 'http://www.adultswim.com/adultswimdynsched/servlet/ScheduleServlet?action=GO&theDate=%s&timeZone=EST'
    # First we grab CN's schedule and put it in a list
    cn_schedule = grab_cnschedule(source_url)

    # CN's last XMLs
    base_url = 'https://www.cartoonnetwork.com/cnschedule/xmlServices/%s.EST.xml'
    cn_schedule = grab_cnxml(base_url)

    # RIP [asched]
    base_url = 'https://www.cartoonnetwork.com/cnschedule/asXml/%s.EST.xml'
    as_schedule = grab_asxml(base_url)

    # [as] schedule api
    # NOTE: IS FIXED TO START AT 8PM
    base_url = 'https://www.adultswim.com/api/schedule/onair?days=7'
    try:
        as_schedule = grab_asjson(base_url)
    except timeout_decorator.timeout_decorator.TimeoutError:
        log('as', 'Function timed out')
    except Exception:
        em = Webhook(config.get('discord', 'issues'), msg='Could not fetch [as]')
        em.add_field(name='Exception', value=traceback.format_exc(), inline=False)
        em.post()
        log('as', 'Could not fetch [as]')

    # [as] schedule page (up to octobreber 2023)
    try:
        as_schedule = grab_ashtml()
    except timeout_decorator.timeout_decorator.TimeoutError:
        log('as', 'Function timed out')
    except Exception:
        em = Webhook(config.get('discord', 'issues'), msg='Could not fetch [as]')
        em.add_field(name='Exception', value=traceback.format_exc(), inline=False)
        em.post()
        log('as', 'Could not fetch [as]')

    # ngtv source
    base_url = 'https://time.ngtv.io/v1/rundown?instance=as-east&startTime=0&endTime=2147483647'
    cn_schedule = grab_ngtv(base_url)

    # DEPRECATED: We merge and order the schedules
    saved_schedule = merge_schedules({
        'cn': cn_schedule,
        # 'zap': zap_schedule,
        # 'tvguide': tvguide_schedule,
        'as': as_schedule
    })

    # DEPRECATED: Then we generate an all.json file
    gen_file(saved_schedule)

    return(cn_schedule, as_schedule)
