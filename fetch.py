import configparser
import contextlib
import json
import math
import re
import sys
import traceback
from datetime import UTC, datetime, timedelta
from pathlib import Path
from time import sleep

import better_exceptions
import crayons
import requests
import timeout_decorator
from dateutil import parser, tz
from lxml import html
from PIL import Image, ImageDraw, ImageFont
from pytz import timezone
from sqlitedict import SqliteDict

from utils.discord_hooks import Webhook
from utils.fix_showname import fix_showname
from utils.id_shows import IdShows
from utils.show_colors import ShowColors

better_exceptions.MAX_LENGTH = None
THIS_DIR = Path(__file__).parent
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)

config = configparser.ConfigParser()
config.read(THIS_DIR.joinpath("config.ini"))
trash = None

cache = None

as_has_5pm_starting = 1693202400  # August 28th 2023 - 6am UTC
as_has_7pm_starting = 1682920800  # May 1st 2023 - 6am UTC
cn_has_8pm_starting = 1664172000  # Sep 26 2022 - 6am UTC
cn_has_8pm_ending = 1672124400  # Dec 27 2022 - 6am UTC
cn_has_8pm_wd_starting = 0  # 0 = Monday, 6 = Sunday
cn_has_8pm_wd_ending = 5  # Including

cn_last = {
    "ts": 0,
    "date": "",
}
zap_last = {
    "ts": 0,
    "date": "",
}
tvguide_last = {
    "ts": 0,
    "date": "",
}
as_last = {
    "ts": 0,
    "date": "",
}
ngtv_last = {
    "ts": 0,
    "date": "",
}
last_group = ""

colors = ShowColors(config)
parse_id = IdShows(config)
et = timezone("US/Eastern")
tzinfos = {
    "EDT": tz.gettz("US/Eastern"),
    "EST": tz.gettz("US/Eastern"),
    "CDT": tz.gettz("US/Central"),
    "CST": tz.gettz("US/Central"),
    "MDT": tz.gettz("US/Mountain"),
    "MST": tz.gettz("US/Mountain"),
    "PDT": tz.gettz("US/Pacific"),
    "PST": tz.gettz("US/Pacific"),
}

# Windows doesn't support the same format
strftime_format = "%-I:%M %p"
if sys.platform == "win32":
    strftime_format = "%#I:%M %p"

rheaders = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36",
}


def log(group, text):
    global last_group

    # If we're changing from a previous group, do a line break
    if last_group != group:
        print("")

    last_group = group

    sys.stdout.write("\x1b[2K\r[{} - {}] {}".format(
        crayons.blue(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        crayons.blue(group.rjust(10)),
        crayons.green(text.ljust(40)),
    ))


def upload_schedule(payload):
    if not config.get("backend", "url"):
        return

    r = requests.post(
        config.get("backend", "url"),
        headers={"X-Api-Key": config.get("backend", "key")},
        verify=False,
        data=json.dumps(payload))

    if r.status_code != requests.codes.ok:
        print("\nError when uploading " + payload["date"] + ", error code " + str(r.status_code))
        print(r.text)

    if config.get("backend", "url_secondary", fallback=None):
        s = requests.post(
            config.get("backend", "url_secondary"),
            headers={"X-Api-Key": config.get("backend", "key")},
            verify=False,
            data=json.dumps(payload))

        if s.status_code != requests.codes.ok:
            print("\nError when uploading " + payload["date"] + " to secondary, error code " + str(s.status_code))
            print(s.text)


def pretty(text):
    # This strips and removes tabs, new lines and (double) spaces from our string
    # Should work most of the time
    text = text.replace("\t", "")
    text = text.replace("\n", "")
    text = text.replace("\\t", "")
    text = text.replace("\\n", "")
    text = re.sub(" +", " ", text)
    text = text.strip()
    if text.startswith('"'):
        text = text[1:]
    if text.endswith('"'):
        text = text[:-1]
    return text.strip()


def fetch_cache_title(url, program_id, api_key):
    # Fetch title from TVGuide programDetails
    title = "-"

    with contextlib.suppress(Exception):
        title = cache.get(str(program_id), "-")

    if title == "-":
        try:
            # Fetch and save
            log("tvguide", "Looking up titles...")
            fetch_url = url.format(program_id, api_key)
            r = requests.get(fetch_url, headers=rheaders)

            if r.status_code < 300:
                json = r.json()
                title = json["data"]["item"]["episodeTitle"]

                cache[str(program_id)] = title
                cache.commit()
        except Exception as e:
            print("\n Error when fetching title from TVGuide (" + str(program_id) + ")")
            print(e)

    return title


def gen_img(data):
    size_x = 550
    size_y = len(data["schedule"]) * 17 + 70
    img = Image.new("RGB", (size_x, size_y), (255, 255, 255))
    d = ImageDraw.Draw(img)
    p = 40

    date_str = datetime.fromtimestamp(data["schedule"][0]["timestamp"]).astimezone(et).strftime("%A %b. %d, %Y")

    src = ""
    if data["source"] == "Cartoon Network":
        src = " / CN"
    elif data["source"] == "Adult Swim":
        src = " / [as]"
    elif data["source"] == "Zap2it":
        src = " / CN via Zap2it"
    elif data["source"] == "TVGuide":
        src = " / CN via TVGuide"

    d.text((15, 5), date_str + src, fill=(0, 0, 0),
           font=ImageFont.truetype(str(THIS_DIR.joinpath("arial.ttf")), 25))

    for i in data["schedule"]:
        time = i["time"]
        show = i["show"]
        title = i["title"]

        if len(time) == 7:
            time = "  " + time

        try:
            if title is not None and len(title) > 2:
                if (len(show) + len(title)) > 55:
                    s = 50 - len(show)
                    title = title[:s] + "…"
                title = "(" + title + ")"
            else:
                title = ""
        except:
            pass

        txt = "%s: %s %s" % (time, show, title)
        d.text((5, p), txt, fill=(0, 0, 0),
               font=ImageFont.truetype(str(THIS_DIR.joinpath("arial.ttf")), 15))
        p += 17

    d.rectangle([(0, size_y - 20), (size_x, size_y)], fill=(0, 0, 0))

    now_date = datetime.now(UTC).strftime("%b. %d, %Y %H:%M (UTC)")

    d.text((5, size_y - 17), "Data fetched " + now_date, fill=(255, 255, 255),
           font=ImageFont.truetype(str(THIS_DIR.joinpath("arial.ttf")), 10))

    d.text((size_x - 110, size_y - 17), "schedule.ctoon.network", fill=(255, 255, 255),
           font=ImageFont.truetype(str(THIS_DIR.joinpath("arial.ttf")), 10))

    img.save(str(THIS_DIR.joinpath("schedule.png")))

    try:
        r = requests.post(
            config.get("image_upload", "url"),
            headers={
                "x-api-key": config.get("image_upload", "api_key"),
            },
            verify=False,
            files={
                "file[]": Path.open(THIS_DIR.joinpath("schedule.png"), "rb"),
            })
        json = r.json()
        return json["url"]
    except Exception:
        em = Webhook(config.get("discord", "issues"),
                     msg="Could not upload image\n```\n" + str(traceback.format_exc()) + "\n```")
        # em.add_field(name="Exception", value=traceback.format_exc(), inline=False)
        em.post()
        log("webhook", "Could not upload image!\n")
        return None


def send_webhook(scheds):
    # Send webhook if new content
    d_url = config.get("discord", "url")

    if not d_url:
        return

    log("webhook", "Checking [adult swim]...")
    if "as" in scheds and int(config["script"]["as_last"]) < as_last["ts"]:
        try:
            shows = {}

            for i in scheds["as"][as_last["date"]]["schedule"]:
                s = i["show"]
                if s in shows:
                    shows[s] += 1
                else:
                    shows[s] = 1

            em = Webhook(d_url, color=1)
            em.set_title(title="Schedule for " + as_last["date"],
                         url="https://schedule.ctoon.network/view/" + as_last["date"])
            em.set_author(name="[adult swim]",
                          icon="https://z.cdn.turner.com/adultswim/big/img/chat-www/icons/as_chat_512x512.png")
            log("webhook", "Uploading [adult swim] image...")
            em.set_image(gen_img(scheds["as"][as_last["date"]]))
            log("webhook", "Posting [adult swim]...")
            em.post()

            config["script"]["as_last"] = str(as_last["ts"])
        except:
            log("webhook-error", "[adult swim] crashed")

    log("webhook", "Checking CN...")
    if "cn" in scheds and int(config["script"]["cn_last"]) < cn_last["ts"]:
        try:
            shows = {}
            cn_top = ""

            for i in scheds["cn"][cn_last["date"]]["schedule"]:
                s = i["show"]
                if s in shows:
                    shows[s] += 1
                else:
                    shows[s] = 1

            co = 0
            for i in sorted(shows.items(), key=lambda x: x[1], reverse=True):
                s_ss = " slots: " if int(i[1]) != 1 else " slot: "
                cn_top += str(i[1]) + s_ss + str(i[0]) + " \n"
                co += 1
                if co >= 5:
                    break

            em = Webhook(d_url, color=15466892)
            em.set_title(title="Schedule for " + cn_last["date"],
                         url="https://schedule.ctoon.network/view/" + cn_last["date"])
            em.set_author(name="Cartoon Network",
                          icon="https://www.cartoonnetwork.com/static/images/global/ogLogo.png")
            em.add_field(name="Top " + str(co), value=cn_top)
            log("webhook", "Uploading CN image...")
            em.set_image(gen_img(scheds["cn"][cn_last["date"]]))
            log("webhook", "Posting CN...")
            em.post()

            config["script"]["cn_last"] = str(cn_last["ts"])
        except:
            log("webhook-error", "CN crashed")

    log("webhook", "Checking Zap...")
    if "zap" in scheds and int(config["script"]["zap_last"]) < zap_last["ts"]:
        try:
            shows = {}
            zap_top = ""

            for i in scheds["zap"][zap_last["date"]]["schedule"]:
                s = i["show"]
                if s in shows:
                    shows[s] += 1
                else:
                    shows[s] = 1

            co = 0
            for i in sorted(shows.items(), key=lambda x: x[1], reverse=True):
                s_ss = " slots: " if int(i[1]) != 1 else " slot: "
                zap_top += str(i[1]) + s_ss + str(i[0]) + " \n"
                co += 1
                if co >= 5:
                    break

            em = Webhook(d_url, color=11337645)
            em.set_title(title="Schedule for " + zap_last["date"],
                         url="https://schedule.ctoon.network/view/" + zap_last["date"])
            em.set_author(name="Zap2it",
                          icon="https://safe.rita.moe/zbi8bTMf.png")
            em.add_field(name="Top " + str(co), value=zap_top)
            log("webhook", "Uploading Zap image...")
            em.set_image(gen_img(scheds["zap"][zap_last["date"]]))
            log("webhook", "Posting Zap...")
            em.post()

            config["script"]["zap_last"] = str(zap_last["ts"])
        except:
            log("webhook-error", "Zap crashed")

    log("webhook", "Checking TVGuide...")
    if "tvguide" in scheds and int(config["script"]["tvguide_last"]) < tvguide_last["ts"]:
        try:
            shows = {}
            tvguide_top = ""

            for i in scheds["tvguide"][tvguide_last["date"]]["schedule"]:
                s = i["show"]
                if s in shows:
                    shows[s] += 1
                else:
                    shows[s] = 1

            co = 0
            for i in sorted(shows.items(), key=lambda x: x[1], reverse=True):
                s_ss = " slots: " if int(i[1]) != 1 else " slot: "
                tvguide_top += str(i[1]) + s_ss + str(i[0]) + " \n"
                co += 1
                if co >= 5:
                    break

            em = Webhook(d_url, color=14029827)
            em.set_title(title="Schedule for " + tvguide_last["date"],
                         url="https://schedule.ctoon.network/view/" + tvguide_last["date"])
            em.set_author(name="TVGuide",
                          icon="https://www.tvguide.com/a/www/img/apple-touch-icon-precomposed.png")
            em.add_field(name="Top " + str(co), value=tvguide_top)
            log("webhook", "Uploading TVGuide image...")
            em.set_image(gen_img(scheds["tvguide"][tvguide_last["date"]]))
            log("webhook", "Posting TVGuide...")
            em.post()

            config["script"]["tvguide_last"] = str(tvguide_last["ts"])
        except:
            log("webhook-error", "TVGuide crashed")

    log("webhook", "Done!")


@timeout_decorator.timeout(300)
def grab_wme_nextdata(url):
    # Get the schedule from the NextJS page
    global cn_last, as_last
    cn_schedule = {}
    as_schedule = {}

    log("cn/as", "Fetching...")
    r = requests.get(url, headers=rheaders)

    if r.status_code < 300:
        log("cn/as", "Looking for the JSON...")
        tree = html.fromstring(r.text)
        next_data = tree.xpath('//script[@id="__NEXT_DATA__"]/text()')[0]
        g_sched = json.loads(next_data)["props"]["pageProps"]["groupedSchedule"]

        for parsed_date, slot in g_sched.items():
            log("cn/as", "Parsing " + parsed_date + "...")

            for episode in slot:
                start_datetime = parser.parse(episode["eastern"], tzinfos=tzinfos)
                start_ts = start_datetime.timestamp()

                show_name = fix_showname(pretty(episode["seriesTitle"]))
                episode_name = pretty(episode["show"]["episode"]["episodeTitle"])

                if show_name == "Family Weekend Movies":
                    continue

                # If it's not between 6am and 5pm (exclusive) (ET) then ignore it's [Adult Swim]
                max_hour = 17
                # Before [as] taking 7pm
                if (start_ts < as_has_7pm_starting):
                    max_hour = 20
                # Before [as] taking 5pm
                elif (start_ts < as_has_7pm_starting):
                    max_hour = 19
                # If CN has 8pm
                elif (start_ts > cn_has_8pm_starting and
                    start_ts < cn_has_8pm_ending and
                    start_datetime.weekday() >= cn_has_8pm_wd_starting and
                    start_datetime.weekday() <= cn_has_8pm_ending):
                    max_hour = 21

                source ="Cartoon Network"
                if not (start_datetime.hour >= 6 and start_datetime.hour < max_hour):
                    source = "Adult Swim"

                length_minutes = int(episode["slotLength"])
                end_datetime = start_datetime + timedelta(minutes=length_minutes)
                slots = math.ceil(length_minutes / 15)
                if slots < 1:
                    slots = 1

                if source == "Cartoon Network":
                    # Init the dict if it doesn't exists
                    if parsed_date not in cn_schedule:
                        cn_schedule[parsed_date] = {
                            "date": parsed_date,
                            "source": "Cartoon Network",
                            "schedule": [],
                        }

                    # Add all the details to our schedule list
                    cn_schedule[parsed_date]["schedule"].append({
                        "time": start_datetime.strftime(strftime_format).lower(),
                        "date": parsed_date,
                        "show": show_name,
                        "title": episode_name,
                        "colors": colors.get(show_name),
                        "timestamp": start_ts,
                        "timestamp_end": end_datetime.timestamp(),
                        "minutes": length_minutes,
                        "slots": slots,
                    })

                    # Save latest saved date
                    if int(end_datetime.timestamp()) > cn_last["ts"]:
                        cn_last = {
                            "ts": int(end_datetime.timestamp()),
                            "date": parsed_date,
                        }
                else:
                    # Init the dict if it doesn't exists
                    if parsed_date not in as_schedule:
                        as_schedule[parsed_date] = {
                            "date": parsed_date,
                            "source": "Adult Swim",
                            "schedule": [],
                        }

                    # Add all the details to our schedule list
                    as_schedule[parsed_date]["schedule"].append({
                        "time": start_datetime.strftime(strftime_format).lower(),
                        "date": parsed_date,
                        "show": show_name,
                        "title": episode_name,
                        "colors": colors.get(show_name),
                        "timestamp": start_ts,
                        "timestamp_end": end_datetime.timestamp(),
                        "minutes": length_minutes,
                        "slots": slots,
                    })

                    # Save latest saved date
                    if int(end_datetime.timestamp()) > as_last["ts"]:
                        as_last = {
                            "ts": int(end_datetime.timestamp()),
                            "date": parsed_date,
                        }


        # And now that we have everything we need,
        # let's upload and return our list
        for parsed_date in cn_schedule:
            # Skip if first item is not 6:00 am (it's incomplete)
            if cn_schedule[parsed_date]["schedule"][0]["time"] != "6:00 am":
                continue

            log("cn/as", "Uploading CN " + parsed_date + "...")
            upload_schedule(cn_schedule[parsed_date])

        for parsed_date in as_schedule:
            # Skip if first item is 12:00 am (it's incomplete)
            if as_schedule[parsed_date]["schedule"][0]["time"] == "12:00 am":
                continue

            # If last item is 12:00 am, set as_last to the previous day (it's incomplete)
            if as_schedule[parsed_date]["schedule"][-1]["time"] == "12:00 am":
                previous_day = parser.parse(as_last["date"] + " 00:00:00") - timedelta(days=1)
                last_item = as_schedule[previous_day.strftime("%Y-%m-%d")]["schedule"][-1]
                as_last = {
                    "ts": int(last_item["timestamp"]),
                    "date": last_item["date"],
                }

            log("cn/as", "Uploading AS " + parsed_date + "...")
            upload_schedule(as_schedule[parsed_date])

    log("cn/as", "Done!")
    return (cn_schedule, as_schedule)


@timeout_decorator.timeout(300)
def grab_zapschedule(url):
    # Get the schedule from the "hidden" API, too
    global zap_last, trash
    json = None
    schedule = {}
    continue_looking = True
    last_time = None

    today = datetime.now(UTC)
    today = today.replace(hour=6, minute=0, second=0, microsecond=0)

    ts = int(today.timestamp())

    # Now, we'll go throught each day
    while continue_looking:
        chan_id = None
        first_retry = False

        log("zap", "Fetching API at timestamp " + str(ts))
        try:
            r = requests.get(url + str(ts), headers=rheaders)

            # In case we break
            if r.status_code == 502:
                print("\n 502 when fetching Zap2it (" + str(ts) + "), waiting...")
                sleep(30)
                continue

            json = r.json()
        except requests.exceptions.ConnectionError as e:
            log("zap", "/!\\ Can't reach Zap2it right now. We're not doing it.")
            trash = e
            return schedule
        except Exception as e:
            if first_retry:
                print("\n Exception when fetching Zap2it (" + str(ts) + "), skipping...")
                print(e)
                first_retry = False
                ts += 7200  # 2 hours later
                sleep(5)
            else:
                print("\n Exception when fetching Zap2it (" + str(ts) + "), trying again in 30s...")
                print(e)
                first_retry = True
                sleep(30)

            continue

        first_retry = False

        if ("channels" not in json):
            # Nothing more to load
            continue_looking = False
            break

        for cid, i in enumerate(json["channels"]):
            if i["callSign"] == "TOON":
                chan_id = cid
                break

        if chan_id is None:
            print("Didn't find TOON")
            break

        events = json["channels"][chan_id]["events"]

        # Get a YYYY-MM-DD version of the date
        parsed_date = datetime.fromtimestamp(ts).astimezone(et).strftime("%Y-%m-%d")
        # Init the list and the increment counter
        if parsed_date not in schedule:
            schedule[parsed_date] = {
                "date": parsed_date,
                "source": "Zap2it",
                "schedule": [],
            }

        log("zap", "Parsing " + parsed_date)

        for slot in events:
            show_name = fix_showname(slot["program"]["title"])
            episode_name = slot["program"]["episodeTitle"]

            # Replace None by an empty string
            if episode_name is None:
                episode_name = ""

            # Prevent duplicates
            if slot["startTime"] == last_time:
                continue

            last_time = slot["startTime"]

            # Parse UTC date
            start_datetime = parser.parse(slot["startTime"])
            end_datetime = parser.parse(slot["endTime"])

            # to ET
            start_et_datetime = start_datetime.astimezone(et)
            end_et_datetime = end_datetime.astimezone(et)

            # to timestamp
            start_ts = int(start_datetime.timestamp())
            end_ts = int(end_datetime.timestamp())
            length_minutes = int(slot["duration"])

            # Set next timestamp
            ts = end_ts

            # If it's not between 6am and 5pm (exclusive) (ET) then ignore it's [Adult Swim]
            max_hour = 17
            # Before [as] taking 7pm
            if (start_ts < as_has_7pm_starting):
                max_hour = 20
            # Before [as] taking 5pm
            elif (start_ts < as_has_5pm_starting):
                max_hour = 19
            # If CN has 8pm
            elif (start_ts > cn_has_8pm_starting and
                  start_ts < cn_has_8pm_ending and
                  start_et_datetime.weekday() >= cn_has_8pm_wd_starting and
                  start_et_datetime.weekday() <= cn_has_8pm_ending):
                max_hour = 21

            if not (start_et_datetime.hour >= 6 and start_et_datetime.hour < max_hour):
                continue

            slots = math.ceil(length_minutes / 15)
            if slots < 1:
                slots = 1

            # Add all the details to our schedule list
            schedule[parsed_date]["schedule"].append({
                "time": start_et_datetime.strftime(strftime_format).lower(),
                "date": parsed_date,
                "show": show_name,
                "title": episode_name,
                "colors": colors.get(show_name),
                "timestamp": start_ts,
                "timestamp_end": end_ts,
                "minutes": length_minutes,
                "slots": slots,
            })

            # Save date if ends at least at 8pm ET (before May 1st '23), 5pm (after August 28th '23) or 7pm (after May 1st '23)
            if ((start_ts < as_has_7pm_starting and end_et_datetime.hour >= 20) or
                (start_ts > as_has_5pm_starting and end_et_datetime.hour >= 17) or
                (start_ts > as_has_7pm_starting and end_et_datetime.hour >= 19)):
                zap_last = {
                    "ts": end_ts,
                    "date": parsed_date,
                }

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log("zap", "Uploading " + parsed_date + "...")
        upload_schedule(schedule[parsed_date])

    log("zap", "Done!")
    return schedule


@timeout_decorator.timeout(300)
def grab_tvguideschedule(url, titles_url, api_key):
    # Get the schedule from their v1 xapi
    global tvguide_last, trash
    json = None
    schedule = {}
    continue_looking = True
    prev_start = 0

    today = datetime.now(UTC)
    today = today.replace(hour=6, minute=0, second=0, microsecond=0)

    ts = int(today.timestamp())

    # Now, we'll go throught each day
    while continue_looking:
        log("tvguide", "Fetching API at timestamp " + str(ts))
        try:
            fetch_url = url.format(ts, api_key)
            r = requests.get(fetch_url, headers=rheaders)

            # In case we break
            if r.status_code == 502:
                print("\n 502 when fetching TVGuide (" + str(ts) + "), waiting...")
                sleep(30)
                continue

            json = r.json()
        except requests.exceptions.ConnectionError as e:
            log("tvguide", "/!\\ Can't reach TVGuide right now. We're not doing it.")
            trash = e
            return schedule
        except Exception as e:
            print("\n Exception when fetching TVGuide (" + str(ts) + "), skipping...")
            print(e)
            ts += 3840 * 60  # 3840 minutes later
            fetch_url = url.format(ts)
            sleep(5)
            continue

        program = None

        if "data" not in json:
            em = Webhook(config.get("discord", "issues"),
                         msg="'data' not found is TVGuide API response:\n```\n" + str(json) + "\n```")
            em.post()
            break

        if len(json["data"]["items"]) < 2:
            continue_looking = False
            break

        for item in json["data"]["items"]:
            if item["channel"]["name"] == "TOON":
                program = item["programSchedules"]
                break

        if program is None or len(program) < 2:
            # print("\n We didn't get TOON")
            continue_looking = False
            break

        # Next page URL
        fetch_url = json["links"]["next"]["href"]

        for slot in program:
            # Get a YYYY-MM-DD version of the date
            if "startTime" not in slot:
                continue

            if prev_start >= slot["startTime"]:
                continue

            prev_start = slot["startTime"]
            parsed_date = datetime.fromtimestamp(slot["startTime"]).astimezone(et).strftime("%Y-%m-%d")

            show_name = fix_showname(slot["title"])
            episode_name = fetch_cache_title(titles_url, slot["programId"], api_key)
            start_ts = int(slot["startTime"])
            end_ts = int(slot["endTime"])

            # Parse UTC date
            start_datetime = datetime.fromtimestamp(start_ts)
            end_datetime = datetime.fromtimestamp(end_ts)

            # to ET
            start_et_datetime = start_datetime.astimezone(et)
            end_et_datetime = end_datetime.astimezone(et)

            length_minutes = math.ceil((end_ts - start_ts) / 60)

            # If it's not between 6am and 5pm (exclusive) (ET) then ignore it's [Adult Swim]
            max_hour = 17
            # Before [as] taking 7pm
            if (start_ts < as_has_7pm_starting):
                max_hour = 20
            # Before [as] taking 5pm
            elif (start_ts < as_has_7pm_starting):
                max_hour = 19
            # If CN has 8pm
            elif (start_ts > cn_has_8pm_starting and
                  start_ts < cn_has_8pm_ending and
                  start_et_datetime.weekday() >= cn_has_8pm_wd_starting and
                  start_et_datetime.weekday() <= cn_has_8pm_ending):
                max_hour = 21

            if not (start_et_datetime.hour >= 6 and start_et_datetime.hour < max_hour):
                continue

            slots = math.ceil(length_minutes / 15)
            if slots < 1:
                slots = 1

            # Init the list and the increment counter
            if parsed_date not in schedule:
                schedule[parsed_date] = {
                    "date": parsed_date,
                    "source": "TVGuide",
                    "schedule": [],
                }

            # Add all the details to our schedule list
            schedule[parsed_date]["schedule"].append({
                "time": start_et_datetime.strftime(strftime_format).lower(),
                "date": parsed_date,
                "show": show_name,
                "title": episode_name,
                "colors": colors.get(show_name),
                "timestamp": start_ts,
                "timestamp_end": end_ts,
                "minutes": length_minutes,
                "slots": slots,
            })

            # Save date if ends at least at 8pm ET (before May 1st '23), 5pm (after August 28th '23) or 7pm (after May 1st '23)
            if ((start_ts < as_has_7pm_starting and end_et_datetime.hour >= 20) or
                (start_ts > as_has_5pm_starting and end_et_datetime.hour >= 17) or
                (start_ts > as_has_7pm_starting and end_et_datetime.hour >= 19)):
                tvguide_last = {
                    "ts": end_ts,
                    "date": parsed_date,
                }

        ts += 3840 * 60  # 3840 minutes later
        sleep(5)

    # And now that we have everything we need,
    # let's upload and return our list
    for parsed_date in schedule:
        log("tvguide", "Uploading " + parsed_date + "...")
        upload_schedule(schedule[parsed_date])

    log("tvguide", "Done!")
    return schedule


if __name__ == "__main__":
    cache = SqliteDict(THIS_DIR.joinpath("cache.sqlite"))

    cn_schedule = {}
    zap_schedule = {}
    tvguide_schedule = {}
    as_schedule = {}

    # Use the NextJS schedule from its __NEXT_DATA__
    base_url = "https://tnets-dvs-schedule.wme-digital.com/?network=CN&grouping=broadcast"
    try:
        (cn_schedule, as_schedule) = grab_wme_nextdata(base_url)
    except timeout_decorator.timeout_decorator.TimeoutError:
        log("cn/as", "Function timed out")
    except Exception:
        em = Webhook(config.get("discord", "issues"),
                     msg="Could not fetch CN/AS\n```\n" + str(traceback.format_exc()) + "\n```")
        # em.add_field(name="Exception", value=traceback.format_exc(), inline=False)
        em.post()
        log("cn/as", "Could not fetch CN/AS")

    # Using Zap2it's API to get their grid, set to load 2 hours (15 broke?) of programing
    base_url = "https://tvschedule.zap2it.com/api/grid?timespan=2&headendId=KY16593&device=X&country=USA&time="
    try:
        zap_schedule = grab_zapschedule(base_url)
    except timeout_decorator.timeout_decorator.TimeoutError:
        log("zap", "Function timed out")
    except Exception:
        em = Webhook(config.get("discord", "issues"),
                     msg="Could not fetch Zap2it\n```\n" + str(traceback.format_exc()) + "\n```")
        # em.add_field(name="Exception", value=traceback.format_exc(), inline=False)
        em.post()
        log("zap", "Could not fetch Zap2it")

    # Using TVGuides's API to get their schedule
    base_url = "https://internal-prod.apigee.fandom.net/v1/xapi/tvschedules/tvguide/9100001138/web?start={}&duration=3840&apiKey={}"
    titles_url = "https://internal-prod.apigee.fandom.net/v1/xapi/tvschedules/tvguide/programdetails/{}/web?apiKey={}"
    try:
        tvguide_schedule = grab_tvguideschedule(base_url, titles_url, config.get("tvg", "key"))
    except timeout_decorator.timeout_decorator.TimeoutError:
        log("tvguide", "Function timed out")
    except Exception:
        em = Webhook(config.get("discord", "issues"),
                     msg="Could not fetch TVGuide\n```\n" + str(traceback.format_exc()) + "\n```")
        # em.add_field(name="Exception", value=traceback.format_exc(), inline=False)
        em.post()
        log("tvguide", "Could not fetch TVGuide")

    # Send webhook if needed
    send_webhook({
        "cn": cn_schedule,
        "as": as_schedule,
        "zap": zap_schedule,
        "tvguide": tvguide_schedule,
    })

    log("all", "Saving config...")
    with Path.open(THIS_DIR.joinpath("config.ini"), "w") as configfile:
        config.write(configfile)

    cache.close()

    log("all", "Notify missing colors on Discord")
    colors.send_report()

    log("all", "Done!")

    print("\nLast error:")
    print(trash)

    # Ending line
    print("")
