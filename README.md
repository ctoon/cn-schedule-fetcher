# CTOON Schedule Fetcher
> Get schedule data from various sources


<details>
<summary>Install pyenv</summary>

- `apt install -y git curl build-essential libreadline-gplv2-dev libssl-dev libbz2-dev libsqlite3-dev zlib1g-dev`
- Follow the pyenv doc to install + add to the `$PATH`: https://github.com/pyenv/pyenv-installer
- `source ~/.bashrc`

</details>

<details>
<summary>Install uv (optional)</summary>

This makes installing dependencies and virtualenvs faster.  
Follow the uv doc to install + add to the `$PATH`: https://github.com/astral-sh/uv

</details>

<details>
<summary>Get the code and deps</summary>

- `git clone https://git.rita.moe/ctoon/cn-schedule-fetcher.git`
- `cd cn-schedule-fetcher`
- `pyenv install`
- (optional) `uv venv && source .venv/bin/activate`
- `pip install .` or `uv pip install .`

</details>

<details>
<summary>Copy, edit config</summary>

- `cp config.ini.dist config.ini`
- `$EDITOR config.ini` and add your Discord webhook URL + your chibisafe file upload details

</details>

<details>
<summary>Run</summary>

**Run locally:**
- `python fetch.py` and look at it run

**Run in a cron:**
- `crontab -e`
- Add a line, replace the path with the correct one:  
  `5 * * * * bash -c "cd /path/to/cn-schedule-fetcher && python fetch.py > cron.log 2>&1"`

**Run in Docker (with its cron):**
- `docker pull git.rita.moe/ctoon/cn-schedule-backend:latest`
- `docker run --rm -d --name "cn-schedule-fetcher" -v "$(pwd)/config.ini:/app/config.ini" git.rita.moe/ctoon/cn-schedule-fetcher:latest`

</details>


### External ressources
`discord_hooks.py` is from https://github.com/kyb3r/Discord-Hooks
